//
//  ChooseItemCell.swift
//  BarCode
//
//  Created by Developer on 29.12.2020.
//

import UIKit

class ChooseItemCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    
    // MARK: - Functions
    func setupCell() {
        cardView.layer.borderColor = UIColor.clear.cgColor
        cardView.layer.borderWidth = 1
        cardView.layer.cornerRadius = 12
        cardView.clipsToBounds = true
        cardView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        itemTitleLabel.tintColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            cardView.backgroundColor = #colorLiteral(red: 0.1882352941, green: 0.4588235294, blue: 0.8666666667, alpha: 1)
        } else {
            cardView.backgroundColor = .white
        }
    }
    
}
