//
//  AddNewPhotoCell.swift
//  BarCode
//
//  Created by Developer on 24.12.2020.
//

import UIKit

class AddNewPhotoCell: UICollectionViewCell {
    
    // MARK: - Properties
    let addButton = UIButton()
	var imageUrl: String!

    // MARK: - Functions

    func setupAddButton() {
        addButton.setTitle("+", for: .normal)
        addButton.setTitleColor(#colorLiteral(red: 0.2352941176, green: 0.7647058824, blue: 0.9921568627, alpha: 1), for: .normal)
        addButton.layer.borderColor = #colorLiteral(red: 0.2352941176, green: 0.7647058824, blue: 0.9921568627, alpha: 1)
        addButton.layer.borderWidth = 1
        addButton.layer.cornerRadius = 12
        contentView.addSubview(addButton)
    }
}
