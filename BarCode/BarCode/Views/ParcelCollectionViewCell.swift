//
//  ParcelCollectionViewCell.swift
//  BarCode
//
//  Created by Developer on 20.12.2020.
//

import UIKit

class ParcelCollectionViewCell: UICollectionViewCell {

    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    } ()
	
	let noSyncedImageView: UIImageView = {
		let imageView = UIImageView()
		imageView.contentMode = .scaleAspectFit
		imageView.clipsToBounds = true
		imageView.translatesAutoresizingMaskIntoConstraints = false
		return imageView
	} ()

    let idLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let customerNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let numberOfCustomerCellLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 15)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()


    override init(frame: CGRect) {
        super.init(frame: .zero)
        contentView.addSubview(imageView)
        contentView.addSubview(idLabel)
        contentView.addSubview(customerNameLabel)
        contentView.addSubview(numberOfCustomerCellLabel)
		contentView.addSubview(noSyncedImageView)

        setupImageView()
        setupIdLabel()
        setupCustomerNameLabel()
        setupNumberOfCustomerCellLabel()
		setupNoSyncedImageView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupImageView() {
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
    }
	
	func setupNoSyncedImageView() {
		noSyncedImageView.image = #imageLiteral(resourceName: "diagonal-line")
		noSyncedImageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
		noSyncedImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
		noSyncedImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
		noSyncedImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
	}

    func setupIdLabel() {
        idLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 5).isActive = true
        idLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        idLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
    }

    func setupCustomerNameLabel() {
        customerNameLabel.topAnchor.constraint(equalTo: idLabel.bottomAnchor, constant: 5).isActive = true
        customerNameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        customerNameLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
    }

    func setupNumberOfCustomerCellLabel() {
        numberOfCustomerCellLabel.topAnchor.constraint(equalTo: customerNameLabel.bottomAnchor, constant: 5).isActive = true
        numberOfCustomerCellLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        numberOfCustomerCellLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
    }
}
