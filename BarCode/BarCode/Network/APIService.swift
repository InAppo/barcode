//
//  APIService.swift
//  BarCode
//
//  Created by Developer on 19.12.2020.
//

import Foundation
import Alamofire
import SwiftyJSON

struct CustomError: Error {

	var title: String?
	var code: Int
	var errorDescription: String? { return _description }
	var failureReason: String? { return _description }

	private var _description: String

	init(title: String?, description: String, code: Int) {
		self.title = title ?? "Error"
		self._description = description
		self.code = code
	}
}

enum APIErrors: Error {
    case customError
    case serverError
}

extension APIErrors: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .customError:
            return NSLocalizedString("Введен неверный логин или пароль", comment: "APIErrors")
        case .serverError:
            return NSLocalizedString("Проверьте Ваше интернет-соединение", comment: "APIErrors")
        }
    }
}

typealias Handler = (Swift.Result<Any?, APIErrors>) -> Void

class APIService {
    static let shared = APIService()
    private init() { }

    func loginRequest(login: LoginModel, completion: @escaping Handler) {
        let url: URL = URL(string: "http://52.72.194.18/api/account/login")!
        let headers: HTTPHeaders = [.contentType("application/x-www-form-urlencoded")]
        AF.request(url, method: .post, parameters: login, headers: headers).response { response in
            switch response.result {
            case .success(let data):
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                    if response.response?.statusCode == 200 {
                        guard let json = json else { return }
                        completion(.success(json["token"]))
                    } else {
                        completion(.failure(.customError))
                    }
                } catch {
                    completion(.failure(.customError))
                }
            case .failure(_):
                completion(.failure(.serverError))
            }
        }
    }

    func getAllPackages(completion: @escaping Handler) {
        let urlString: String = "http://52.72.194.18/api/account/packages"
        guard let token = UserDefaults.standard.string(forKey: "token") else { return }
        guard let url = URL(string: urlString) else { return }
        let headers: HTTPHeaders = [.contentType("application/x-www-form-urlencoded"),
                                    .authorization(bearerToken: token)]

        AF.request(url, method: .get, headers: headers).response { response in
			print(response.result)
            switch response.result {
            case .success(_):
                if let jsonData = response.data,
                   let parcels = try? JSONDecoder().decode(Parcels.self, from: jsonData) {
                    let resultPackages = parcels.packages
					if !resultPackages.isEmpty {
						let swiftyJson = JSON(jsonData)
						BCDataManager.shared.saveLocalPackages(json: swiftyJson)
					}
                    completion(.success(resultPackages.sorted(by: {$0.id > $1.id})))
                }
            case .failure(_):
                completion(.failure(.serverError))
			}
		}.cURLDescription { (curl) in
			print(curl)
		}
    }

    func getPackageById(id: String, completion: @escaping (Swift.Result<Package, APIErrors>) -> Void) {
        let urlString: String = "http://52.72.194.18/api/account/packages/id/\(id)"
        guard let token = UserDefaults.standard.string(forKey: "token") else { return }
        guard let url = URL(string: urlString) else { return }
        let headers: HTTPHeaders = [.contentType("application/x-www-form-urlencoded"),
                                    .authorization(bearerToken: token)]

        AF.request(url, method: .get, headers: headers).response { response in
            switch response.result {
            case .success(_):
                if let jsonData = response.data,
                   let parcels = try? JSONDecoder().decode(CurrentPackage.self, from: jsonData) {
                    let package = parcels.package
                    completion(.success(package))
                }
            case .failure(_):
                completion(.failure(.serverError))
            }
        }.cURLDescription { (curl) in
			print(curl)
		}
    }

	func updatePackageById(id: String, withCellName: Bool = false, package: NewPackage, completion: @escaping (Error?) -> Void) {
        let urlString: String = "http://52.72.194.18/api/account/packages/\(id)"
        guard let token = UserDefaults.standard.string(forKey: "token") else { return }
        guard let url = URL(string: urlString) else { return }
        var headers: HTTPHeaders = [.authorization(bearerToken: token)]
        headers.add(HTTPHeader(name: "Content-Type", value: "multipart/form-data"))
        var parameters: Parameters = ["store": package.store,
                                      "warehouse_cell_id": package.warehouseCellId,
                                      "uuid": package.uuid,
                                      "recipient": package.recipient,
                                      "weight": package.weight]
		if withCellName {
			parameters.removeValue(forKey: "warehouse_cell_id")
			parameters["cell_name"] = package.cellName
		}

		print("parameters = \(parameters)")
        AF.upload(multipartFormData: { multiFormData in
            for (key, value) in parameters {
                if value is String {
                    multiFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                } else if value is Int {
                    multiFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
			if package.packagePhoto != nil {
				multiFormData.append(package.packagePhoto!, withName: "package_photo", fileName: "packagePhoto.jpg", mimeType: "image/jpeg")
			}
			if package.labelPhoto != nil {
				multiFormData.append(package.labelPhoto!, withName: "label_photo", fileName: "labelPhoto.jpg", mimeType: "image/jpeg")
			}
            if package.additionalPhoto1 != nil {
                multiFormData.append(package.additionalPhoto1!, withName: "additional_photo_1", fileName: "additional_photo_1.jpg", mimeType: "image/jpeg")
            }
            if package.additionalPhoto2 != nil {
                multiFormData.append(package.additionalPhoto2!, withName: "additional_photo_2", fileName: "additional_photo_2.jpg", mimeType: "image/jpeg")
            }
            if package.additionalPhoto3 != nil {
                multiFormData.append(package.additionalPhoto3!, withName: "additional_photo_3", fileName: "additional_photo_3.jpg", mimeType: "image/jpeg")
            }
            if package.additionalPhoto4 != nil {
                multiFormData.append(package.additionalPhoto4!, withName: "additional_photo_4", fileName: "additional_photo_4.jpg", mimeType: "image/jpeg")
            }

        }, to: url, method: .post, headers: headers).responseJSON { response in
            switch response.result {
            case .success(_):
                completion(nil)
            case .failure(let error):
                completion(error)
            }
		}.cURLDescription { (curl) in
			print(curl)
		}
    }

    func getDataForCreatePackage(completion: @escaping (DataForCreatePackege) -> Void) {
        let urlString: String = "http://52.72.194.18/api/account/common/filter"
        guard let token = UserDefaults.standard.string(forKey: "token") else { return }
        guard let url = URL(string: urlString) else { return }
        let headers: HTTPHeaders = [ .authorization(bearerToken: token)]

        AF.request(url, method: .get, headers: headers).response { response in
            switch response.result {
            case .success(_):
                if let jsonData = response.data,
                   let items = try? JSONDecoder().decode(DataForCreatePackege.self, from: jsonData) {
					let swiftyJson = JSON(jsonData)
					BCDataManager.shared.saveDataForCreatePackage(json: swiftyJson)
                    completion(items)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }.cURLDescription { (curl) in
			print(curl)
		}
    }

    func createPackage(package: NewPackage, withCellName: Bool, completion: @escaping ((Int?, Error?)) -> Void) {
        let urlString: String = "http://52.72.194.18/api/account/packages/create"
        guard let token = UserDefaults.standard.string(forKey: "token") else { return }
        guard let url = URL(string: urlString) else { return }
        var headers: HTTPHeaders = [ .authorization(bearerToken: token)]
		headers.add(HTTPHeader(name: "Content-Type", value: "multipart/form-data"))
        var parameters: Parameters = ["store": package.store,
                                      "warehouse_cell_id": package.warehouseCellId,
                                      "uuid": package.uuid,
                                      "recipient": package.recipient,
                                      "weight": package.weight]
        if withCellName {
            parameters.removeValue(forKey: "warehouse_cell_id")
            parameters["cell_name"] = package.cellName
        }
        AF.upload(multipartFormData: { multiFormData in
            for (key, value) in parameters {
                if value is String {
                    multiFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                } else if value is Int {
                    multiFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            multiFormData.append(package.packagePhoto!, withName: "package_photo", fileName: "packagePhoto.jpg", mimeType: "image/jpeg")
            multiFormData.append(package.labelPhoto!, withName: "label_photo", fileName: "labelPhoto.jpg", mimeType: "image/jpeg")
            if package.additionalPhoto1 != nil {
                multiFormData.append(package.additionalPhoto1!, withName: "additional_photo_1", fileName: "additional_photo_1.jpg", mimeType: "image/jpeg")
            }
            if package.additionalPhoto2 != nil {
                multiFormData.append(package.additionalPhoto2!, withName: "additional_photo_2", fileName: "additional_photo_2.jpg", mimeType: "image/jpeg")
            }
            if package.additionalPhoto3 != nil {
                multiFormData.append(package.additionalPhoto3!, withName: "additional_photo_3", fileName: "additional_photo_3.jpg", mimeType: "image/jpeg")
            }
            if package.additionalPhoto4 != nil {
                multiFormData.append(package.additionalPhoto4!, withName: "additional_photo_4", fileName: "additional_photo_4.jpg", mimeType: "image/jpeg")
            }

        }, to: url, method: .post, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                do {
                    guard let data = response.data else {
                        return
                    }
					print(value)
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
					if let value = json?["success"] as? Int, value == 0 , let message =  json?["message"] as? String {
						let error = CustomError(title: message, description: message, code: 100)
						completion((nil, error))
						return
					} else {
						if let id = json?["package_id"] as? Int {
							completion((id, nil))
						}
						
					}
                } catch let error {
                    print(error.localizedDescription)
					completion((nil, error))
                }
            case .failure(let error):
				completion((nil, error))
            }
        }.cURLDescription { (curl) in
			print(curl)
		}
    }
}
