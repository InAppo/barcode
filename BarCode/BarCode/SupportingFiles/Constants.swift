//
//  Constants.swift
//  BarCode
//
//  Created by Developer on 17.05.2021.
//

import UIKit

struct Constants {
	static let circleViewAlpha: CGFloat = 0.7
	static let rectangleViewAlpha: CGFloat = 0.3
	static let shapeViewAlpha: CGFloat = 0.3
	static let rectangleViewCornerRadius: CGFloat = 10.0
	
	struct UI {
		static var needToReloadData = false
	}
}
