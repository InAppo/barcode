//
//  AppDelegate.swift
//  BarCode
//
//  Created by Developer on 18.12.2020.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		configureRechability()
		IQKeyboardManager.shared.enable = true
		RunLoop.current.run(until: Date(timeIntervalSinceNow: 1.0))
		FirebaseApp.configure()
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        let storyboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let navController = UINavigationController()
        let isLogin: Bool = UserDefaults.standard.bool(forKey: "isLogin")
//		forceShowScanViewController()
        if isLogin {
            let mainScreenVC = storyboard.instantiateViewController(withIdentifier: "MainTabBarController")
            navController.viewControllers = [mainScreenVC]
            self.window?.rootViewController = navController
        } else {
            let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            navController.viewControllers = [loginVC]
            self.window?.rootViewController = navController
        }
        self.window?.makeKeyAndVisible()
        return true
	}
	
	private func configureRechability() {
		do {
			try Network.reachability = Reachability(hostname: "www.google.com")
		}
		catch {
			switch error as? Network.Error {
			case let .failedToCreateWith(hostname)?:
				print("Network error:\nFailed to create reachability object With host named:", hostname)
			case let .failedToInitializeWith(address)?:
				print("Network error:\nFailed to initialize reachability object With address:", address)
			case .failedToSetCallout?:
				print("Network error:\nFailed to set callout")
			case .failedToSetDispatchQueue?:
				print("Network error:\nFailed to set DispatchQueue")
			case .none:
				print(error)
			}
		}
	}
	
	private func forceShowScanViewController() {
		let storyboard = UIStoryboard(name: "ScanLabel", bundle: nil)
		let scanViewController = storyboard.instantiateInitialViewController()
		self.window?.rootViewController = scanViewController
		self.window?.makeKeyAndVisible()
	}
}

