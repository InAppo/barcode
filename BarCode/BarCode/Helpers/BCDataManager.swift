//
//  BCDataManager.swift
//  BarCode
//
//  Created by Developer on 08.05.2021.
//

import UIKit
import SwiftyJSON

class BCDataManager {
	
	static var shared: BCDataManager = {
		let instance = BCDataManager()
		return instance
	}()
	
	let createPackegeDataFileName = "CreatePackegeData"
	let localSyncedPackagesFileName = "LocalPackages"
	let localNotSyncedPackagesFileName = "NotSyncedLocalPackages"
	
	private init() {}
	
	func getDocumentsDirectory() -> URL {
		let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		return paths[0]
	}
	
	//	MARK: - Data for create package
	
	func removeLocalDataForCreatePackage() {
		let filePath = getDocumentsDirectory().appendingPathComponent(createPackegeDataFileName)
		if let _ = try? Data(contentsOf: filePath, options: .mappedIfSafe) {
			do {
				try FileManager.default.removeItem(atPath: filePath.path)
			} catch {
				debugPrint(error)
			}
		}
	}
	
	func saveDataForCreatePackage(json: JSON) {
		let str = json.description
		guard let data = str.data(using: String.Encoding.utf8) else { return }
		do {
			let documentDirectory = getDocumentsDirectory()
			let fileURL = documentDirectory.appendingPathComponent(createPackegeDataFileName)
			try data.write(to: fileURL)
		} catch {
			debugPrint(error)
		}
	}
	
	func getDataForCreatePackage() -> DataForCreatePackege? {
		let fileUrl = getDocumentsDirectory().appendingPathComponent(createPackegeDataFileName)
		if let data = try? Data(contentsOf: fileUrl, options: .mappedIfSafe) {
			if let jsonResult = try? JSON(data: data) {
				let decoder = JSONDecoder()
				if let createPackegeData = try? decoder.decode(DataForCreatePackege.self, from: jsonResult.rawData()) {
					return createPackegeData
				}
			}
		}
		return nil
	}
	
	
//	MARK: - Package
	
	func getLocalPackages() -> Parcels? {
		let fileUrl = getDocumentsDirectory().appendingPathComponent(localSyncedPackagesFileName)
		if let data = try? Data(contentsOf: fileUrl, options: .mappedIfSafe) {
			if let jsonResult = try? JSON(data: data) {
				let decoder = JSONDecoder()
				if let parcels = try? decoder.decode(Parcels.self, from: jsonResult.rawData()) {
					return parcels
				}
			}
		}
		return nil
	}
	
	func saveLocalPackages(json: JSON) {
		let str = json.description
		guard let data = str.data(using: String.Encoding.utf8) else { return }
		do {
			let documentDirectory = getDocumentsDirectory()
			let fileURL = documentDirectory.appendingPathComponent(localSyncedPackagesFileName)
			try data.write(to: fileURL)
		} catch {
			debugPrint(error)
		}
	}

//	MARK: - Store packagas offline
	
	func saveNotSyncedPackages(parcels: [NewPackage]) {
		let encoder = JSONEncoder()
		guard let encoded = try? encoder.encode(parcels) else { return }
		do {
			let documentDirectory = getDocumentsDirectory()
			let fileURL = documentDirectory.appendingPathComponent(localNotSyncedPackagesFileName)
			try encoded.write(to: fileURL)
		} catch {
			debugPrint(error)
		}
	}
	
	func getNotSyncedPackages() -> [NewPackage]? {
		let fileUrl = getDocumentsDirectory().appendingPathComponent(localNotSyncedPackagesFileName)
		if let data = try? Data(contentsOf: fileUrl, options: .mappedIfSafe) {
			if let jsonResult = try? JSON(data: data) {
				let decoder = JSONDecoder()
				if let parcels = try? decoder.decode([NewPackage].self, from: jsonResult.rawData()) {
					return parcels
				}
			}
		}
		return nil
	}
	
}
