//
//  CustomScrollView.swift
//  BarCode
//
//  Created by Ihor Vozhdai on 05.04.2021.
//

import UIKit

class CustomScrollView: UIScrollView {

    var containerView: UIView?

    init(view: UIView, contentViewSize: CGSize, bounces: Bool = true) {
        super.init(frame: .zero)
        frame = view.bounds
        contentSize = contentViewSize
        autoresizingMask = .flexibleHeight
        showsHorizontalScrollIndicator = true
        self.bounces = bounces

        let containerView = UIView()
        containerView.frame.size = contentViewSize

        addSubview(containerView)
        self.containerView = containerView
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
