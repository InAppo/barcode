//
//  ProgressManager.swift
//  MLVisionExample
//
//  Created by Developer on 26.07.2020.
//  Copyright © 2020 Google Inc. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

final class ProgressManager: NSObject {
    
	class func show(message: String? = nil) {
        if !NVActivityIndicatorPresenter.sharedInstance.isAnimating {
            DispatchQueue.main.async {
				let activityData = ActivityData(message: message)
                NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            }
        }
    }
    
    class func success() {
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        }
    }
}
