//
//  ChoicedItemEnum.swift
//  BarCode
//
//  Created by Developer on 19.01.2021.
//

import Foundation

enum ChoosedItemNameEnum: String {
    case store = "магазин"
    case recipients = "получателя"
    case cells = "ячейку клиента"
}
