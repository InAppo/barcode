//
//  NewPackageModel.swift
//  BarCode
//
//  Created by Developer on 05.01.2021.
//

import Foundation

struct NewPackage: Codable {
    var store: String
    var warehouseCellId: Int
    var cellName: String?
    var uuid: String
    var recipient: String
    var weight: String
    var packagePhoto: Data?
    var labelPhoto: Data?
    var additionalPhoto1: Data?
    var additionalPhoto2: Data?
    var additionalPhoto3: Data?
    var additionalPhoto4: Data?
	
	var packagePhotoUrl: String?
	var labelPhotoUrl: String?
	var additionalPhoto1Url: String?
	var additionalPhoto2Url: String?
	var additionalPhoto3Url: String?
	var additionalPhoto4Url: String?
	
	var localUUID: String = UUID().uuidString
	var isEdit: Bool = false
	var editId: Int?
	var createdAt: String?

    private enum CodingKeys : String, CodingKey {
        case store
        case warehouseCellId = "warehouse_cell_id"
        case cellName = "cell_name"
        case uuid
        case recipient
        case weight
        case packagePhoto = "package_photo"
        case labelPhoto = "label_photo"
        case additionalPhoto1 = "additional_photo_1"
        case additionalPhoto2 = "additional_photo_2"
        case additionalPhoto3 = "additional_photo_3"
        case additionalPhoto4 = "additional_photo_4"
		
		case localUUID = "local_uuid"
		case isEdit = "is_edit"
		case editId = "edit_id"
		case packagePhotoUrl
		case labelPhotoUrl
		case additionalPhoto1Url
		case additionalPhoto2Url
		case additionalPhoto3Url
		case additionalPhoto4Url
		case createdAt
    }
}
