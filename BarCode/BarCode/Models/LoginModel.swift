//
//  LoginModel.swift
//  BarCode
//
//  Created by Developer on 19.12.2020.
//

import UIKit

struct LoginModel: Codable {
    var login: String
    var password: String
}
