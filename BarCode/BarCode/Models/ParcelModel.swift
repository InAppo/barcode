//
//  ParcelModel.swift
//  BarCode
//
//  Created by Developer on 20.12.2020.
//

import Foundation

struct Parcels: Codable {
    var success: Bool
    var packages: [Package]

    init(success: Bool, packages: [Package]) {
        self.success = success
        self.packages = packages
    }
}

struct CurrentPackage: Codable {
    var success: Bool
    var package: Package

    init(success: Bool, package: Package) {
        self.success = success
        self.package = package
    }
}

struct Package: Codable {
    var id: Int
    var warehouseName: String?
    var warehouseCellName: String?
    var storeName: String?
    var uuid: String?
    var recipientName: String
    var packagePhoto: String
	var compressedPackagePhoto: String?
	var compressedLabelPhoto: String?
    var labelPhoto: String?
    var additionalPhoto1: String?
    var additionalPhoto2: String?
    var additionalPhoto3: String?
    var additionalPhoto4: String?
	var compressedAdditionalPhoto1: String?
	var compressedAdditionalPhoto2: String?
	var compressedAdditionalPhoto3: String?
	var compressedAdditionalPhoto4: String?
    var weight: String?
    var createdAt: String?
	
	private enum CodingKeys : String, CodingKey {
		case id
        case warehouseName = "warehouse_name"
		case warehouseCellName = "warehouse_cell_name"
        case storeName = "store_name"
        case uuid
		case recipientName = "recipient_name"
		case packagePhoto = "package_photo"
		case compressedPackagePhoto = "compressed_package_photo"
		case compressedLabelPhoto = "compressed_label_photo"
        case labelPhoto = "label_photo"
        case additionalPhoto1 = "additional_photo_1"
        case additionalPhoto2 = "additional_photo_2"
        case additionalPhoto3 = "additional_photo_3"
        case additionalPhoto4 = "additional_photo_4"
		case compressedAdditionalPhoto1 = "compressed_additional_photo_1"
		case compressedAdditionalPhoto2 = "compressed_additional_photo_2"
		case compressedAdditionalPhoto3 = "compressed_additional_photo_3"
		case compressedAdditionalPhoto4 = "compressed_additional_photo_4"
        case weight
        case createdAt = "created_at"
	}
}
