//
//  PackagesItemModel.swift
//  BarCode
//
//  Created by Developer on 29.12.2020.
//

import Foundation

struct DataForCreatePackege: Codable {
    var success: Bool
    var stores: [Item]
    var cellsTypes: [CellsType]
    var cells: [Item]
    var recipients: [Recipient]

    private enum CodingKeys : String, CodingKey {
        case success
        case stores
        case cellsTypes = "cells_types"
        case cells
        case recipients
    }
}

    // MARK: - Cell
    struct Item: Codable {
        var id: Int
        var name: String
    }

    // MARK: - CellsType
    struct CellsType: Codable, Equatable {
        var id: Int
        var type: String
    }

    // MARK: - Recipient
    struct Recipient: Codable {
        var recipient: String
    }

