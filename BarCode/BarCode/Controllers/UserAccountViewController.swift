//
//  UserAccountViewController.swift
//  BarCode
//
//  Created by Developer on 21.01.2021.
//

import UIKit

class UserAccountViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = "Личный кабинет"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.systemBlue]
    }

}
