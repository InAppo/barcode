//
//  MainTabBarController.swift
//  BarCode
//
//  Created by Developer on 20.01.2021.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
		self.delegate = self
        navigationController?.navigationBar.isHidden = true
    }
	
	private func scanLabel(_ tabBarController: UITabBarController) {
		guard let navigationVC = (tabBarController.selectedViewController as? UINavigationController) else { return }
		let addNewVC = storyboard?.instantiateViewController(identifier: "AddNewParcelViewController") as! AddNewParcelViewController
        addNewVC.stateOfController = .newPackage
		let storyboard = UIStoryboard(name: "ScanLabel", bundle: nil)
		let scanVC = storyboard.instantiateInitialViewController() as! ScanLabelViewController
		scanVC.delegate = addNewVC
		navigationVC.setViewControllers([addNewVC, scanVC], animated: true)
	}

	func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
		let index = tabBarController.selectedIndex
		if index == 1 {
			scanLabel(tabBarController)
			return
		}
	}
}
