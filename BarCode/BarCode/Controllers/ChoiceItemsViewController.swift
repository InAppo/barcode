//
//  ChoiceItemsViewController.swift
//  BarCode
//
//  Created by Developer on 28.12.2020.
//

import UIKit

protocol ChooseItemsProtocol: AnyObject {
	func finishChoosing()
}

class ChoiceItemsViewController: UIViewController {

    // MARK: - Properties
    var data: [Item] = []
    var choicedItems = [String]()
    var choosingItemName: String = ""
    var cellsId = [Int]()
	var cellsName = [String]()
    let searchController = UISearchController(searchResultsController: nil)
    var stateOfPreviousController: AddNewParcelState?
	
	weak var parentController: AddNewParcelViewController?

    private var filteredItems = [Item]()
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }

    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    } ()

    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
		setupArrays()
        self.hideKeyboardWhenTappedAround()
        setupSearchController()
        view.addSubview(tableView)

        setupTableView()
		
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
	
	private func setupArrays() {
		if choosingItemName == ChoosedItemNameEnum.cells.rawValue {
			let arrayCells = isFiltering ? filteredItems : data
			for item in arrayCells {
				cellsName.append(item.name)
				cellsId.append(item.id)
			}
		}
	}
    
    func setupNavigationBar() {
        self.navigationItem.title = "Выбрать \(choosingItemName)"
        navigationItem.searchController = self.searchController
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.systemBlue]
    }

    func finishChoiceItem() {
        if !choicedItems.isEmpty {
            guard let lastItemOfChoicedItems = choicedItems.last else { return }
            switch choosingItemName {
            case ChoosedItemNameEnum.store.rawValue:
				parentController?.newPackage.store = lastItemOfChoicedItems
                if stateOfPreviousController == AddNewParcelState.editPackage {
                    AddNewParcelViewController.storeForEditeblePackage = lastItemOfChoicedItems
                }
            case ChoosedItemNameEnum.recipients.rawValue:
				parentController?.newPackage.recipient = lastItemOfChoicedItems
                if stateOfPreviousController == AddNewParcelState.editPackage {
                    AddNewParcelViewController.recipientForEditeblePackage = lastItemOfChoicedItems
                }
            case ChoosedItemNameEnum.cells.rawValue:
                if stateOfPreviousController == AddNewParcelState.editPackage {
                    AddNewParcelViewController.cellForEditeblePackage = lastItemOfChoicedItems
                }
            default:
                break
            }
        }
        navigationController?.popViewController(animated: true)
    }

    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        definesPresentationContext = true
    }

    func setupTableView() {
        let nib = UINib(nibName: "ChooseItemCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ChooseItemCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: topbarHeight).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }
}

extension ChoiceItemsViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredItems.count
        }
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseItemCell", for: indexPath) as! ChooseItemCell
        cell.selectionStyle = .none
        cell.setupCell()

        var item: Item
        if isFiltering {
            item = filteredItems[indexPath.row]
        } else {
            item = data[indexPath.row]
        }

        if choosingItemName == ChoosedItemNameEnum.cells.rawValue {
            cell.itemTitleLabel.text = item.name
        } else {
            cell.itemTitleLabel.text = item.name
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ChooseItemCell else { return }
        cell.setSelected(true, animated: true)
        choicedItems.append(cell.itemTitleLabel.text!)
		if choosingItemName == ChoosedItemNameEnum.cells.rawValue {
			parentController?.newPackage.warehouseCellId = cellsId[indexPath.row]
            if stateOfPreviousController == AddNewParcelState.editPackage {
                AddNewParcelViewController.cellIdForEditeblePackage = cellsId[indexPath.row]
				AddNewParcelViewController.cellForEditeblePackage = cellsName[indexPath.row]
            }
		}
        finishChoiceItem()
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ChooseItemCell else { return }
        choicedItems = choicedItems.filter{ $0 != cell.itemTitleLabel.text! }
    }
}

extension ChoiceItemsViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }

    private func filterContentForSearchText(_ searchText: String) {
        filteredItems = data.filter({ (subCategory: Item) -> Bool in
            return subCategory.name.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}
