
import UIKit
import MLKitBarcodeScanning
import Vision
import Firebase

enum ScanType {
	case barcode
	case recipient
	case shop
}

protocol ScanLabelDelegate: AnyObject {
	func scanDidCompleate(barcode: String, recepient: String, shop: String, image: UIImage?)
}

class ScanLabelViewController: UIViewController, UINavigationControllerDelegate {
	
	// MARK: - IBOutlets
	@IBOutlet private weak var containerForScrollView: UIView!
	@IBOutlet private weak var photoCameraButton: UIBarButtonItem!
	@IBOutlet private weak var btnDone: UIButton!
	@IBOutlet private weak var tfBarCode: UITextField!
	@IBOutlet private weak var lblShop: UILabel!
	@IBOutlet private weak var tfRecipient: UITextField!
	@IBOutlet private weak var tfShop: UITextField!
	@IBOutlet private weak var typeSwitch: UISegmentedControl!
	
	private var imageScrollView: ImageScrollView!
	
	private var scanType: ScanType = .barcode {
		didSet {
			switch scanType {
			case .barcode:
				currentTextField = tfBarCode
			case .recipient:
				currentTextField = tfRecipient
			case .shop:
				currentTextField = tfShop
				tfRecipient.isHidden = true
				tfBarCode.isHidden = true
				typeSwitch.isHidden = true
				lblShop.isHidden = false
				tfShop.isHidden = false
			}
		}
	}
	
	private var currentTextField: UITextField!
    private lazy var vision = Vision.vision()
    private var path = UIBezierPath()
    private var initialLocation = CGPoint.zero
    private var finalLocation = CGPoint.zero
    private var shapeLayer = CAShapeLayer()
    private var textButtons: [CustomButton] = []
    private var choosenButtons: [CustomButton] = []
    private var imagePicker = UIImagePickerController()
    
    // Image counter.
    private var currentImage = 0
	
	private var vnBarCodeDetectionRequest : VNDetectBarcodesRequest{
			let request = VNDetectBarcodesRequest { (request,error) in
				guard let results = request.results else { return }
				
				for result in results {
					if let barcode = result as? VNBarcodeObservation {
						if let desc = barcode.payloadStringValue {
							DispatchQueue.main.async {
								if self.tfBarCode.text!.isEmpty {
									self.tfBarCode.text = desc
									return
								}
							}
						}
					}
					}
			}
			return request
		}
	
	
	
	var imageSelected: UIImage!
	var delegate: ScanLabelDelegate?
	var isShopScan: Bool = false
    
    var scale: CGFloat = 2 {
        didSet {
            imageScrollView.imageZoomView.subviews.forEach { (view) in
                var border: CGFloat!
                switch scale {
                case 1...2:
                    border = 3
                case 2...3:
                    border = 2
                case 3...4:
                    border = 1
                default:
                    border = 3
                }
                if view is CustomButton {
                    view.layer.borderWidth = border
                }
            }
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
		if isShopScan {
			scanType = .shop
		} else {
			scanType = .barcode
		}
		navigationItem.backBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: nil)
		tfBarCode.delegate = self
		tfShop.delegate = self
		tfRecipient.delegate = self
		btnDone.layer.cornerRadius = 10
        setupImageScrollView()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        let isCameraAvailable = UIImagePickerController.isCameraDeviceAvailable(.front)
            || UIImagePickerController.isCameraDeviceAvailable(.rear)
        if !isCameraAvailable {
            photoCameraButton.isEnabled = false
        }

        setupView()
		if let imageSelected = imageSelected {
			imageScrollView.set(image: imageSelected)
			detectImageText()
		}
		
		setupNavigationBar()
        openCameraIfNeeded()
    }
	
	func setupNavigationBar() {
		self.navigationItem.title = "Сканирование"
		navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.systemBlue]
	}
    
    func setupImageScrollView() {
        imageScrollView = ImageScrollView(frame: containerForScrollView.bounds)
        containerForScrollView.addSubview(imageScrollView)
	
        imageScrollView.translatesAutoresizingMaskIntoConstraints = false
        imageScrollView.topAnchor.constraint(equalTo: containerForScrollView.topAnchor).isActive = true
        imageScrollView.bottomAnchor.constraint(equalTo: containerForScrollView.bottomAnchor).isActive = true
        imageScrollView.trailingAnchor.constraint(equalTo: containerForScrollView.trailingAnchor).isActive = true
        imageScrollView.leadingAnchor.constraint(equalTo: containerForScrollView.leadingAnchor).isActive = true
        imageScrollView.reloadDelegate = self
    }
    
    
    func setupView(){
        self.view.layer.addSublayer(shapeLayer)
        self.shapeLayer.lineWidth = 20
        self.shapeLayer.strokeColor = UIColor.blue.cgColor
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

//        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        navigationController?.navigationBar.isHidden = false
    }

    func openCameraIfNeeded() {
        guard
            (UIImagePickerController.isCameraDeviceAvailable(.front)
                || UIImagePickerController
                    .isCameraDeviceAvailable(.rear)) && imageSelected == nil
            else {
                return
        }
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true)
    }
    
    // MARK: - IBActions
	
	@IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
		clearSelected()
		if sender.selectedSegmentIndex == 0 {
			scanType = .barcode
			tfRecipient.isHidden = true
			tfBarCode.isHidden = false
		} else {
			scanType = .recipient
			tfBarCode.isHidden = true
			tfRecipient.isHidden = false
		}
	}
	
	@IBAction func doneClicked(_ sender: Any) {
		navigationController?.popViewController(animated: true)
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
			self.delegate?.scanDidCompleate(barcode: self.tfBarCode.text!, recepient: self.tfRecipient.text!, shop: self.tfShop.text!, image: self.imageScrollView?.imageZoomView?.image)
		}
		
	}
	
	@IBAction func openPhotoLibrary(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true)
    }
    
    @IBAction func openCamera(_ sender: Any) {
        openCameraIfNeeded()
    }
    
    // MARK: - Private
	
	private func detectImageText() {
		guard let image = imageScrollView.imageZoomView?.image else {
			alert(message: "Please select image")
			return
		}
		clearResults()
		
		let options = VisionCloudTextRecognizerOptions()
		options.modelType = .dense
		options.languageHints = ["ru", "en", "tr"]
		removeDetectionAnnotations()
		imageScrollView.zoomScale = 1
		detectTextInCloud(image: image, options: options)
		
		createVisionRequest(image: image)
	}
	
	func createVisionRequest(image: UIImage) {
		guard let cgImage = image.cgImage else {
			return
		}
		let imageOrientation = getCGOrientationFromUIImage(image)
		let requestHandler = VNImageRequestHandler(cgImage: cgImage, orientation: imageOrientation, options: [:])
		let vnRequests = [vnBarCodeDetectionRequest]
		DispatchQueue.global(qos: .background).async {
			do{
				try requestHandler.perform(vnRequests)
			}catch let error as NSError {
				print("Error in performing Image request: \(error)")
			}
		}
	}
	
	func getCGOrientationFromUIImage(_ image: UIImage) -> CGImagePropertyOrientation {
		// returns que equivalent CGImagePropertyOrientation given an UIImage.
		// This is required because UIImage.imageOrientation values don't match to CGImagePropertyOrientation values
		switch image.imageOrientation {
		case .down:
			return .down
		case .left:
			return .left
		case .right:
			return .right
		case .up:
			return .up
		case .downMirrored:
			return .downMirrored
		case .leftMirrored:
			return .leftMirrored
		case .rightMirrored:
			return .rightMirrored
		case .upMirrored:
			return .upMirrored
		@unknown default:
			return .up
		}
	}

    /// Removes the detection annotations from the annotation overlay view.
    private func removeDetectionAnnotations() {
        imageScrollView.imageZoomView?.subviews.forEach { (view) in
            if view is CustomButton {
                view.removeFromSuperview()
            }
        }
    }
    
    /// Clears the results text view and removes any frames that are visible.
    private func clearResults() {
		currentTextField.text = ""
        textButtons = []
		clearSelected()
    }
	
	private func clearSelected() {
		choosenButtons.forEach { (button) in
			button.layer.borderColor = UIColor.white.cgColor
		}
		choosenButtons = []
	}

    private func updateImageView(with image: UIImage) {
        self.imageScrollView.set(image: image)
    }
    
    private func transformMatrix() -> CGAffineTransform {
        guard let image = imageScrollView.imageZoomView.image else { return CGAffineTransform() }
        let imageViewWidth = imageScrollView.imageZoomView.frame.size.width
        let imageViewHeight = imageScrollView.imageZoomView.frame.size.height
        let imageWidth = image.size.width
        let imageHeight = image.size.height

        let imageViewAspectRatio = imageViewWidth / imageViewHeight
        let imageAspectRatio = imageWidth / imageHeight
        let scale = (imageViewAspectRatio > imageAspectRatio)
            ? imageViewHeight / imageHeight : imageViewWidth / imageWidth

        // Image view's `contentMode` is `scaleAspectFit`, which scales the image to fit the size of the
        // image view by maintaining the aspect ratio. Multiple by `scale` to get image's original size.
        let scaledImageWidth = imageWidth * scale
        let scaledImageHeight = imageHeight * scale
        let xValue = (imageViewWidth - scaledImageWidth) / CGFloat(2.0)
        let yValue = (imageViewHeight - scaledImageHeight) / CGFloat(2.0)

        var transform = CGAffineTransform.identity.translatedBy(x: xValue, y: yValue)
        transform = transform.scaledBy(x: scale, y: scale)
        return transform
    }
    
    private func pointFrom(_ visionPoint: VisionPoint) -> CGPoint {
        return CGPoint(x: CGFloat(visionPoint.x.floatValue), y: CGFloat(visionPoint.y.floatValue))
    }
    
    private func process(_ visionImage: VisionImage, with textRecognizer: VisionTextRecognizer?) {
        ProgressManager.show()
		textRecognizer?.process(visionImage) { [self] text, error in
            ProgressManager.success()
            guard error == nil, let text = text else {
                let errorString = error?.localizedDescription ?? ConstantsEnum.detectionNoResultsMessage
				self.alert(message: errorString)
                return
            }
            // Blocks.
            for block in text.blocks {
                // Lines.
                for line in block.lines {

                    // Elements.
                    for element in line.elements {
                        let transformedRect = element.frame.applying(self.transformMatrix())
                        let button = CustomButton(frame: transformedRect)
                        button.isUserInteractionEnabled = false
                        button.layer.borderWidth = 2.0
                        button.layer.borderColor = UIColor.white.cgColor
                        button.detectedText = element.text
                        self.textButtons.append(button)
                        button.setTitle("", for: .normal)

                        button.addTarget(self, action: #selector(self.buttonAction(_:)), for: .allEvents)
                        button.alpha = 1
                        self.imageScrollView.imageZoomView.addSubview(button)
                        button.isUserInteractionEnabled = true
						self.imageScrollView.zoomToOriginal()
                    }
                }
            }
        }
    }
    
    @objc func buttonAction(_ sender: CustomButton!) {
        if !choosenButtons.contains(sender) {
            sender.layer.borderColor = UIColor.blue.cgColor
            choosenButtons.append(sender)
			currentTextField.text! += " " + sender.detectedText
        }
    }
    
    @objc func buttonActionMove(sender: CustomButton!) {
        if !choosenButtons.contains(sender) {
            sender.layer.borderColor = UIColor.blue.cgColor
            choosenButtons.append(sender)
			currentTextField.text! += " " + sender.detectedText
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let location = touches.first?.location(in: self.view){
            initialLocation = location
        }
    }
    
}

extension ScanLabelViewController: ReloadDelegate {
    func removeAllSubview() {
       
    }
    
    func reloadButtons(scale: CGFloat) {
        self.scale = scale
    }
}
// MARK: - UIImagePickerControllerDelegate

extension ScanLabelViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
    ) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        clearResults()
        if let pickedImage
            = info[
                convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)]
                as? UIImage
        {
            updateImageView(with: pickedImage)
			detectImageText()
        }
        dismiss(animated: true)
    }
	
	func convertFromUIImagePickerControllerInfoKeyDictionary(
		_ input: [UIImagePickerController.InfoKey: Any]
	) -> [String: Any] {
		return Dictionary(uniqueKeysWithValues: input.map { key, value in (key.rawValue, value) })
	}

	func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey)
		-> String {
		return input.rawValue
	}
}


/// Extension of ViewController for On-Device and Cloud detection.
extension ScanLabelViewController {
    
    func alert(message: String, title: String = "", handlerOk: ((UIAlertAction) -> Void)? = nil) {
           DispatchQueue.main.async {
               let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
               let okAction = UIAlertAction(title: "Ok", style: .default, handler: handlerOk)
               alertController.addAction(okAction)
               self.present(alertController, animated: true, completion: nil)
           }
       }
	
    // MARK: - Vision Cloud Detection
    
    func detectTextInCloud(image: UIImage?, options: VisionCloudTextRecognizerOptions) {
        guard let image = image else { return }
        
        // Define the metadata for the image.
        let imageMetadata = VisionImageMetadata()
        imageMetadata.orientation = UIUtilities.visionImageOrientation(from: image.imageOrientation)
        
        // Initialize a VisionImage object with the given UIImage.
        let visionImage = VisionImage(image: image)
        visionImage.metadata = imageMetadata
        
        // [START init_text_cloud]
        var cloudTextRecognizer: VisionTextRecognizer?
		cloudTextRecognizer = vision.cloudTextRecognizer(options: options)
		
        process(visionImage, with: cloudTextRecognizer)
    }
}

extension ScanLabelViewController: UITextFieldDelegate {
	func textFieldShouldClear(_ textField: UITextField) -> Bool {
		clearSelected()
		return true
	}
	func textFieldDidBeginEditing(_ textField: UITextField) {
		clearSelected()
	}
}

// MARK: - Enums

private enum ConstantsEnum {
    static let detectionNoResultsMessage = "No results returned."
    static let sparseTextModelName = "Sparse"
    static let denseTextModelName = "Dense"
}
