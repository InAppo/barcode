//
//  LoginViewController.swift
//  BarCode
//
//  Created by Developer on 18.12.2020.
//


import UIKit

class LoginViewController: UIViewController {

    // MARK: - Properties
    let backView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    } ()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "BarCode"
        label.textColor = #colorLiteral(red: 0.06274509804, green: 0.06274509804, blue: 0.06274509804, alpha: 1)
        label.textAlignment = .center
        label.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 35)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let loginTextField: TextFieldWithPadings = {
        let textField = TextFieldWithPadings()
        textField.attributedPlaceholder = NSAttributedString(string: "Логин",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.systemGray])
        textField.textContentType = .username
		textField.autocapitalizationType = .none
        textField.layer.borderWidth = 1
        textField.layer.borderColor = #colorLiteral(red: 0.7333333333, green: 0.7333333333, blue: 0.7333333333, alpha: 1)
        textField.translatesAutoresizingMaskIntoConstraints = false
		textField.returnKeyType = .next
        return textField
    } ()
	
	let autoCompleateView: AutoCompleateView = {
		let view = AutoCompleateView()
		view.backgroundColor = #colorLiteral(red: 0.7333333333, green: 0.7333333333, blue: 0.7333333333, alpha: 1)
		view.isHidden = true
		view.titleButton.addTarget(self, action: #selector(autoCompleateButtonClicked), for: .touchUpInside)
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	} ()

    let passwordTextField: TextFieldWithPadings = {
        let textField = TextFieldWithPadings()
        textField.isSecureTextEntry = true
        textField.attributedPlaceholder = NSAttributedString(string: "Пароль",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.systemGray])
        textField.textContentType = .password
        textField.layer.borderWidth = 1
        textField.layer.borderColor = #colorLiteral(red: 0.7333333333, green: 0.7333333333, blue: 0.7333333333, alpha: 1)
        textField.translatesAutoresizingMaskIntoConstraints = false
		textField.returnKeyType = .done
        return textField
    } ()

    let rememberMeButton: UIButton = {
        let button = UIButton()
        let selectedImage = UIImage(systemName: "checkmark.square.fill")
        let unselectedImage = UIImage(systemName: "square")
        button.setImage(unselectedImage, for: .normal)
        button.setImage(selectedImage, for: .selected)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(rememberButtonTapped), for: .touchUpInside)
        return button
    } ()

    let rememberMeLable: UILabel = {
        let label = UILabel()
        label.text = "Запомнить данные"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("Войти", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.06274509804, green: 0.06274509804, blue: 0.06274509804, alpha: 1), for: .normal)
        button.layer.cornerRadius = 4
        button.layer.borderColor = #colorLiteral(red: 0.7333333333, green: 0.7333333333, blue: 0.7333333333, alpha: 1)
        button.layer.borderWidth = 1
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        return button
    } ()

    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        navigationController?.navigationBar.isHidden = true
        view.addSubview(backView)
        backView.addSubview(titleLabel)
        backView.addSubview(loginTextField)
        backView.addSubview(passwordTextField)
        backView.addSubview(rememberMeButton)
        backView.addSubview(rememberMeLable)
        backView.addSubview(loginButton)
		backView.addSubview(autoCompleateView)
		loginTextField.delegate = self
		passwordTextField.delegate = self

        setupBackView()
        setupTitleLabel()
        setupLoginTextField()
        setupPasswordTextField()
        setupRememberMeButton()
        setupRememberMeLable()
        setupLoginButton()
		setupAutoCompleateView()
    }

    @objc func rememberButtonTapped() {
        if rememberMeButton.isSelected {
            rememberMeButton.isSelected = false
        } else {
            rememberMeButton.isSelected = true
        }
    }
	
	@objc func autoCompleateButtonClicked() {
		if let entity = autoCompleateView.entity {
			loginTextField.text = entity.login
			passwordTextField.text = entity.password
			autoCompleateView.isHidden = true
		}
	}

    @objc func loginButtonTapped() {
        guard let login = loginTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        let loginModel = LoginModel(login: login, password: password)
        ProgressManager.show()
        if loginTextField.text != "" && passwordTextField.text != "" {
            APIService.shared.loginRequest(login: loginModel) { result in
                switch result {
                case .success(let token):
                    UserDefaults.standard.set(token, forKey: "token")
					UserDefaults.standard.set(true, forKey: "isLogin")
					if self.rememberMeButton.isSelected {
						AutoCompleateManager.shared.saveEntity(entity: AutoCompleateEntity(login: loginModel.login, password: loginModel.password))
					}
                    UserDefaults.standard.synchronize()
                    self.navigationToMainScreen()
                case .failure(let error):
                    let alert = UIAlertController(title: "Ошибка!", message: "\(error.localizedDescription)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                ProgressManager.success()
            }
        } else {
            let alert = UIAlertController(title: "Ошибка!", message: "Логин и пароль обязательны для заполнения", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            ProgressManager.success()
        }
    }

    func navigationToMainScreen() {
        let mainVC = storyboard?.instantiateViewController(identifier: "MainTabBarController") as! MainTabBarController
        navigationController?.pushViewController(mainVC, animated: true)
    }

    func setupBackView() {
        backView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        backView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        backView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/2).isActive = true
        backView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 2/3).isActive = true
    }

    func setupTitleLabel() {
        titleLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: 0).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 0).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: backView.rightAnchor, constant: 0).isActive = true
    }

    func setupLoginTextField() {
        loginTextField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 100).isActive = true
        loginTextField.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 0).isActive = true
        loginTextField.rightAnchor.constraint(equalTo: backView.rightAnchor, constant: 0).isActive = true
        loginTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }

    func setupPasswordTextField() {
        passwordTextField.topAnchor.constraint(equalTo: loginTextField.bottomAnchor, constant: 1).isActive = true
        passwordTextField.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 0).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: backView.rightAnchor, constant: 0).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }

    func setupRememberMeButton() {
        rememberMeButton.imageView!.contentMode = .scaleAspectFill
        rememberMeButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 20).isActive = true
        rememberMeButton.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 0).isActive = true
        rememberMeButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        rememberMeButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
    }

    func setupRememberMeLable() {
        rememberMeLable.centerYAnchor.constraint(equalTo: rememberMeButton.centerYAnchor).isActive = true
        rememberMeLable.leftAnchor.constraint(equalTo: rememberMeButton.rightAnchor, constant: 10).isActive = true
    }
	
	func setupAutoCompleateView() {
		autoCompleateView.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 10).isActive = true
		autoCompleateView.rightAnchor.constraint(equalTo: backView.rightAnchor, constant: -10).isActive = true
		autoCompleateView.heightAnchor.constraint(equalToConstant: 50).isActive = true
		autoCompleateView.topAnchor.constraint(equalTo: loginTextField.bottomAnchor, constant: 0).isActive = true
	}

    func setupLoginButton() {
        loginButton.topAnchor.constraint(equalTo: rememberMeButton.bottomAnchor, constant: 20).isActive = true
        loginButton.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 0).isActive = true
        loginButton.rightAnchor.constraint(equalTo: backView.rightAnchor, constant: 0).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
}

extension LoginViewController: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if let text = textField.text,
		   let textRange = Range(range, in: text) {
			let updatedText = text.replacingCharacters(in: textRange,
													   with: string)
			if textField == loginTextField, let saved = AutoCompleateManager.shared.getEntity(), saved.login.contains(updatedText) {
				autoCompleateView.entity = saved
				autoCompleateView.isHidden = false
			} else {
				autoCompleateView.isHidden = true
			}
		}
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == loginTextField {
			passwordTextField.becomeFirstResponder()
		} else {
			view.endEditing(true)
		}
		autoCompleateView.isHidden = true
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		autoCompleateView.isHidden = true
	}
}

