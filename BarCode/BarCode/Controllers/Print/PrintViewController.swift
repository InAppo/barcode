//
//  PrintViewController.swift
//  BarCode
//
//  Created by Developer on 23.02.2021.
//

import UIKit
import CoreBluetooth

let ZPRINTER_SERVICE_UUID                   = "38EB4A80-C570-11E3-9507-0002A5D5C51B"
let WRITE_TO_ZPRINTER_CHARACTERISTIC_UUID   = "38EB4A82-C570-11E3-9507-0002A5D5C51B"
let READ_FROM_ZPRINTER_CHARACTERISTIC_UUID  = "38EB4A81-C570-11E3-9507-0002A5D5C51B"

// The names used for Notification Center
let ZPRINTER_WRITE_NOTIFICATION        = "WriteNotification" // For sending ZPL
let ZPRINTER_READ_NOTIFICATION         = "ReadNotification"  // For getting response
let ZPRINTER_DIS_NOTIFICATION          = "DISNotification"   // For reading DIS values

// Device Information Service (DIS) of Zebra printer
let ZPRINTER_DIS_SERVICE                    = "180A"
let ZPRINTER_DIS_CHARAC_MODEL_NAME          = "2A24"
let ZPRINTER_DIS_CHARAC_SERIAL_NUMBER       = "2A25"
let ZPRINTER_DIS_CHARAC_FIRMWARE_REVISION   = "2A26"
let ZPRINTER_DIS_CHARAC_HARDWARE_REVISION   = "2A27"
let ZPRINTER_DIS_CHARAC_SOFTWARE_REVISION   = "2A28"
let ZPRINTER_DIS_CHARAC_MANUFACTURER_NAME   = "2A29"

let peripheralIdKey = "peripheral_id"

var defaultPeripheralID: String? {
	get {
		return UserDefaults.standard.string(forKey: peripheralIdKey)
	}
	set {
		UserDefaults.standard.setValue(newValue, forKeyPath: peripheralIdKey)
	}
}

class PrintViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CBCentralManagerDelegate, CBPeripheralDelegate {
	
	@IBOutlet private weak var tableView: UITableView!
	
	private var centralManager: CBCentralManager!
	private var listPeripherals: [CBPeripheral] = []
	private var selectedPeripheral: CBPeripheral?
	private var sendToPrinterString: String = ""
	private var userDefaults = UserDefaults.standard
	private var leftbbi: UIBarButtonItem!
	
	var package: Package!
	
	var newPackage: NewPackage!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		initScaninig()
		setupTableView()
		leftbbi = UIBarButtonItem(title: "Разпечатать", style: .done, target: self, action: #selector(bbiClicked))
		navigationItem.rightBarButtonItem = leftbbi
    }
	
	private func setupTableView() {
		tableView.delegate = self
		tableView.dataSource = self
	}
	
	private func initScaninig() {
		centralManager = CBCentralManager(delegate: self, queue: .main)
		let options: [String: Any] = [CBCentralManagerScanOptionAllowDuplicatesKey:
									  NSNumber(value: false)]
		centralManager?.scanForPeripherals(withServices: nil, options: options)
	}
	
	private func cleanUp() {
		let options: [String: Any] = [CBCentralManagerScanOptionAllowDuplicatesKey:
									  NSNumber(value: false)]
		centralManager.scanForPeripherals(withServices: nil, options: options)
		if let selectedPeripheral = selectedPeripheral {
			centralManager.cancelPeripheralConnection(selectedPeripheral)
		}
		selectedPeripheral = nil
		tableView.reloadData()
	}
	
	


	
	func centralManagerDidUpdateState(_ central: CBCentralManager) {
		switch central.state {
		case .poweredOn:
			centralManager?.scanForPeripherals(withServices: nil, options: [:])
		default:
			break
		}
	}
	
	//	MARK: - IBAction
	
	@objc func bbiClicked() {
		if let selectedPeripheral = selectedPeripheral {
			centralManager.stopScan()
			if let package = package {
				sendToPrinterString = "^XA^FO40,20^APN,60,60^FDID\(package.id)^FS^FO340,20^APN,60,60^FD\(package.warehouseCellName!)^FS^FO40,75^APN,40,40^FD\(package.storeName!)^FS^FO40,118^APN,40,40^FD\(package.weight!) KG^FS^FO40,160^APN,40,40^FD\(package.recipientName)^FS^FO640,30^BQN,2,7^FDMM,AID\(package.id)^FS^XZ"
			}
			centralManager.connect(selectedPeripheral, options: [:])
		}
	}
	
	func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
		print("Discovered \(peripheral.name ?? "")")
		if (RSSI.intValue > -15) {
			return
		}
		if (RSSI.intValue < -70) {
			return
		}
		if !(peripheral.name?.isEmpty ?? true) {
			if !listPeripherals.contains(where: {$0.name == peripheral.name}) {
				listPeripherals.append(peripheral)
				tableView.reloadData()
				if let peripheralID = defaultPeripheralID, peripheral.identifier == UUID(uuidString: peripheralID) {
					selectedPeripheral = peripheral
					bbiClicked()
				}
			}
		}
	}
	
	//	MARK: - CBPeripheralDelegate
	
	func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
		print(#function)
		peripheral.delegate = self
		peripheral.discoverServices([CBUUID(string: ZPRINTER_SERVICE_UUID), CBUUID(string: ZPRINTER_DIS_SERVICE)])
	}
	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
		if let error = error {
			print(error.localizedDescription)
			cleanUp()
		} else {
			for service in peripheral.services ?? [] {
				if service.uuid == CBUUID(string: ZPRINTER_SERVICE_UUID) {
					defaultPeripheralID = peripheral.identifier.uuidString
					peripheral.discoverCharacteristics([CBUUID(string: READ_FROM_ZPRINTER_CHARACTERISTIC_UUID), CBUUID(string: WRITE_TO_ZPRINTER_CHARACTERISTIC_UUID)], for: service)
					return
				}
			}
		}
	}
	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
		for characteristic in service.characteristics ?? []  {
			if characteristic.uuid == CBUUID(string: WRITE_TO_ZPRINTER_CHARACTERISTIC_UUID) {
				if let data = sendToPrinterString.data(using: .utf8) {
//					showToast(message: "Найден сервис печати и отправлено на печать")
					peripheral.writeValue(data, for: characteristic, type: CBCharacteristicWriteType.withResponse)
					navigationController?.popViewController(animated: false)
				}
			}
		}
		
	}
	
	func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
//		showToast(message: "розпечатано")
	}
	
	func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
		cleanUp()
	}
	
//	MARK: - TableView datasource&delegate
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return listPeripherals.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "PrinterTableViewCell") as! PrinterTableViewCell
		let peripheral = listPeripherals[indexPath.row]
		cell.lblTitle.text = peripheral.name
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		selectedPeripheral = listPeripherals[indexPath.row]
	}
	
}
