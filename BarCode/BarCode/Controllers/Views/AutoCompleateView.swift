//
//  AutoCompleateView.swift
//  BarCode
//
//  Created by Developer on 24.01.2021.
//

import UIKit

class AutoCompleateView: UIView {
	
	let titleButton = UIButton()
	
	var entity: AutoCompleateEntity? = nil {
		didSet {
			if let entity = entity {
				titleButton.setTitle(entity.login, for: .normal)
			}
		}
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		addSubview(titleButton)
		titleButton.titleLabel?.minimumScaleFactor = 0.5
		titleButton.titleLabel?.adjustsFontSizeToFitWidth = true
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		titleButton.frame = self.bounds
	}
}
