//
//  ShowImageViewController.swift
//  BarCode
//
//  Created by Developer on 13.03.2021.
//

import UIKit
import Zoomy

class ShowImageViewController: UIViewController {

	@IBOutlet weak var imgView: UIImageView!
	
	var image: UIImage?
	var imageUrl: String?
	
	override func viewDidLoad() {
        super.viewDidLoad()
		if let  image = image {
			imgView.image = image
		} else if let url = imageUrl {
			imgView.sd_setImage(with: URL(string: url)!, completed: { _,_,_,_  in
				
			})
		}
		addZoombehavior(for: imgView)
		
    }

}
