//
//  AddNewParcelViewController.swift
//  BarCode
//
//  Created by Developer on 23.12.2020.
//

import UIKit
import SDWebImage
import SwiftyJSON

enum AddNewParcelState {
  case newPackage
  case showDetailsPackage
  case editPackage
}

protocol Statable {
  func update(state: AddNewParcelState)
}

protocol MainScreenUpdateDelegate: AnyObject {
	func updateWhenAppear()
}

let labelFontSize: CGFloat = 20

class AddNewParcelViewController: UIViewController, Statable {

    // MARK: - Properties
	
	var newPackage: NewPackage!
	
    var stores = [Item]()
	var cellTypes = [CellsType]()
    var cells = [Item]()
    var recipients = [Item]()
    var photos = [UIImage]()
    var imageForPackage: String = ""
    var detailsobjId: String = ""
    var additionalPhotosCompressed: [String] = []
	var additionalPhotosOriginal: [String] = []
	var additionalPhotosDataForEditablePackage: [Data?] = [nil, nil, nil, nil]
	var additionalPhotosForEditablePackage: [UIImage?] = [nil, nil, nil, nil]
	var package: Package?
    var stateOfController: AddNewParcelState?
	var bluetoothManager: BluetoothManager = BluetoothManager()
    static var storeForEditeblePackage: String?
    static var recipientForEditeblePackage: String?
    static var cellForEditeblePackage: String? // name of cell
    static var cellIdForEditeblePackage: Int? // id of cell
    var labelPhotoForEditeblePackage: Data?
    var packagePhotoForEditeblePackage: Data?
    var indexOfClickedImage: Int = 0
	var editedOptionalImageIndex: Int!
	var isEditNotSyncedPackage: Bool = false
	
	var topSpacing: CGFloat = 20
	var collectionViewHeight: CGFloat = 120
	
	weak var delegate: MainScreenUpdateDelegate?

    lazy var contentSize = CGSize(width: self.view.frame.width,
                            height: self.view.frame.height)

    lazy var scrollView: CustomScrollView = {
        let scrollView = CustomScrollView(view: view, contentViewSize: contentSize)
        return scrollView
    } ()

    let storeLabel: UILabel = {
        let label = UILabel()
        label.text = "Магазин"
        label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: labelFontSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let storeSelectButton: UIButton = {
        let button = UIButton()
        button.setTitle("-- >", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(selectStoreTapped), for: .touchUpInside)
        return button
    } ()
	
	let idTitleLabel: UILabel = {
		let label = UILabel()
		label.text = "ID"
		label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: labelFontSize)
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	} ()
	
	let idValueLabel: UILabel = {
		let label = UILabel()
		label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: labelFontSize)
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	} ()

    let barcodeTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Баркод"
        label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: labelFontSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

	let barcodeSelectButton: UIButton = {
		let button = UIButton()
		button.setTitle("-- >", for: .normal)
		button.setTitleColor(.systemBlue, for: .normal)
		button.translatesAutoresizingMaskIntoConstraints = false
		button.addTarget(self, action: #selector(selectBarcodeTapped), for: .touchUpInside)
		return button
	} ()

    let recipientLabel: UILabel = {
        let label = UILabel()
        label.text = "Получатель"
        label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: labelFontSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let recipientSelectButton: UIButton = {
        let button = UIButton()
        button.setTitle("-- >", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(selectRecipientTapped), for: .touchUpInside)
        return button
    } ()

    let cellIdLabel: UILabel = {
        let label = UILabel()
        label.text = "Ячейка клиента"
        label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: labelFontSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let cellSelectButton: UIButton = {
        let button = UIButton()
        button.setTitle("-- >", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(selectCellTapped), for: .touchUpInside)
        return button
    } ()

    let weightLabel: UILabel = {
        let label = UILabel()
        label.text = "Вес"
        label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: labelFontSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let weightTextField: UITextField = {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(string: "Введите вес", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        textField.keyboardType = .decimalPad
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addTarget(self, action: #selector(weightTextFieldDidChange), for: .editingDidEnd)
        return textField
    } ()
	
	let recepientTextField: UITextField = {
		let textField = UITextField()
        let rightParagraphStyle = NSMutableParagraphStyle()
        rightParagraphStyle.alignment = .right
		textField.attributedPlaceholder = NSAttributedString(string: "Получатель", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                                                                                .paragraphStyle: rightParagraphStyle])
		textField.translatesAutoresizingMaskIntoConstraints = false
		textField.addTarget(self, action: #selector(recepientTextFieldDidChange), for: .editingDidEnd)
		return textField
	} ()
	
	let barcodeTextField: UITextField = {
		let textField = UITextField()
        let rightParagraphStyle = NSMutableParagraphStyle()
        rightParagraphStyle.alignment = .right
		textField.attributedPlaceholder = NSAttributedString(string: "Баркод", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                                                                                  .paragraphStyle: rightParagraphStyle])
		textField.translatesAutoresizingMaskIntoConstraints = false
		textField.addTarget(self, action: #selector(barcodeTextFieldDidChange), for: .editingDidEnd)
		return textField
	} ()

    let storeTextField: UITextField = {
        let textField = UITextField()
        let rightParagraphStyle = NSMutableParagraphStyle()
        rightParagraphStyle.alignment = .right
        textField.attributedPlaceholder = NSAttributedString(string: "Магазин", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                                                                            .paragraphStyle: rightParagraphStyle])
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addTarget(self, action: #selector(storeTextFieldDidChange), for: .editingDidEnd)
        return textField
    } ()

    let cellTextField: UITextField = {
        let textField = UITextField()
        let rightParagraphStyle = NSMutableParagraphStyle()
        rightParagraphStyle.alignment = .right
        textField.attributedPlaceholder = NSAttributedString(string: "Ячейка клиента", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                                                                            .paragraphStyle: rightParagraphStyle])
        textField.translatesAutoresizingMaskIntoConstraints = false
		textField.addTarget(self, action: #selector(cellTextFieldDidChange), for: .editingChanged)
        return textField
    } ()

    let printButton: UIButton = {
        let button = UIButton()
        let image = UIImage(systemName: "printer.dotmatrix")
        let config = UIImage.SymbolConfiguration(pointSize: 50)
        button.setImage(image, for: .normal)
		button.addTarget(self, action: #selector(buttonPrintPressed), for: .touchUpInside)
        button.setPreferredSymbolConfiguration(config, forImageIn: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    } ()

    let imagePackageLabelButton: UIButton = {
        let button = UIButton()
        button.setTitle("+", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.2732920647, green: 0.8086070418, blue: 0.9947416186, alpha: 1), for: .normal)
        button.layer.cornerRadius = 12
        button.layer.borderWidth = 1
        button.layer.borderColor = #colorLiteral(red: 0.2732920647, green: 0.8086070418, blue: 0.9947416186, alpha: 1)
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(setImageForPackageLabel), for: .touchUpInside)
        return button
    } ()

    let labelForImagePackageLabel: UILabel = {
        let label = UILabel()
        label.text = "Фото лейбл"
        label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let imagePackageButton: UIButton = {
        let button = UIButton()
        button.setTitle("+", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.2732920647, green: 0.8086070418, blue: 0.9947416186, alpha: 1), for: .normal)
        button.layer.cornerRadius = 12
        button.layer.borderWidth = 1
        button.layer.borderColor = #colorLiteral(red: 0.2732920647, green: 0.8086070418, blue: 0.9947416186, alpha: 1)
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(setImageForPackage), for: .touchUpInside)
        return button
    } ()

    let labelForImagePackage: UILabel = {
        let label = UILabel()
        label.text = "Фото посылки"
        label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    } ()

    let dateLabel: UILabel = {
        let label = UILabel()
        label.text = "Дата и время"
        label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: labelFontSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let dateValueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "AppleSDGothicNeo-Semibold", size: labelFontSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    let dateSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemGray2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    } ()
	
	let idSeparatorView: UIView = {
		let view = UIView()
		view.backgroundColor = .systemGray2
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	} ()

    let firstSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemGray2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    } ()

    let secondSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemGray2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    } ()

    let thirdSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemGray2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    } ()

    let fourstSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemGray2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    } ()

    let fivestSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemGray2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    } ()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
		if newPackage == nil {
			newPackage = NewPackage(store: "", warehouseCellId: 0, cellName: "", uuid: "", recipient: "", weight: "", packagePhoto: Data(), labelPhoto: Data())
		} else {
			fillWithNewPackage(isAllowEdit: false)
		}
		if UIDevice.current.userInterfaceIdiom == .phone {
			if UIScreen.main.nativeBounds.height <= 1334 {
				topSpacing = 12
				collectionViewHeight = 80
			}
		}
        self.weightTextField.delegate = self
		DispatchQueue.main.async {
			self.setDataForCreatePackege()
		}

        self.hideKeyboardWhenTappedAround()
        setupViews()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		additionalPhotosCompressed = []
		additionalPhotosCompressed = []
		if let state = stateOfController {
			update(state: state)
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		self.title = ""
	}
	
	func fillWithNewPackage(isAllowEdit: Bool) {
		weightTextField.text = newPackage.weight
		cellTextField.text = newPackage.cellName ?? "\(newPackage.warehouseCellId)"
		if isAllowEdit {
			cellSelectButton.setTitle("-->", for: .normal)
		} else {
			cellSelectButton.setTitle(newPackage.cellName ?? "\(newPackage.warehouseCellId)", for: .normal)
		}
		dateValueLabel.text = newPackage.createdAt == nil ? "----" : "\(newPackage.createdAt!)"
		idValueLabel.text = newPackage.editId == nil ? "----" : "\(newPackage.editId!)"
		storeTextField.text = newPackage.store
		barcodeTextField.text = newPackage.uuid
		recepientTextField.text = newPackage.recipient
		labelPhotoForEditeblePackage = newPackage.labelPhoto
		packagePhotoForEditeblePackage = newPackage.packagePhoto
		if let labelPhoto = newPackage.labelPhoto {
			imagePackageLabelButton.setImage(UIImage(data: labelPhoto), for: .normal)
		} else {
			let imageUrl = URL(string: newPackage.labelPhotoUrl ?? "")
			imagePackageLabelButton.sd_setImage(with: imageUrl, for: .normal, completed: nil)
		}
		if let packagePhoto = newPackage.packagePhoto {
			imagePackageButton.setImage(UIImage(data: packagePhoto), for: .normal)
		} else {
			let labelPhoto = UIImageView()
			let imageUrl = URL(string: newPackage.packagePhotoUrl ?? "")
			imagePackageButton.sd_setImage(with: imageUrl, for: .normal, completed: nil)
		}
		if let additionalPhoto = newPackage.additionalPhoto1 {
			additionalPhotosForEditablePackage[0] = UIImage(data: additionalPhoto)
		}
		if let additionalPhoto = newPackage.additionalPhoto2 {
			additionalPhotosForEditablePackage[1] = UIImage(data: additionalPhoto)
		}
		if let additionalPhoto = newPackage.additionalPhoto3 {
			additionalPhotosForEditablePackage[2] = UIImage(data: additionalPhoto)
		}
		if let additionalPhoto = newPackage.additionalPhoto4 {
			additionalPhotosForEditablePackage[3] = UIImage(data: additionalPhoto)
		}
		collectionView.reloadData()
	}

    func update(state: AddNewParcelState) {
        switch(state) {
        case .newPackage:
            newPackageConfig()
        case .showDetailsPackage:
            detailsPackageConfig()
        case .editPackage:
            editPackageConfig()
        }
	}
	
	private func setDataForCreatePackege() {
		if let oldDataForPackege = BCDataManager.shared.getDataForCreatePackage() {
			self.stores = oldDataForPackege.stores
			self.cells = oldDataForPackege.cells
			self.cellTypes = oldDataForPackege.cellsTypes
			for item in oldDataForPackege.recipients {
				let item = Item(id: +1, name: item.recipient)
				self.recipients.append(item)
			}
			if self.stateOfController == AddNewParcelState.newPackage {
				self.updateAccordingToCellType()
			}
		}
		APIService.shared.getDataForCreatePackage { items in
			self.stores = items.stores
			self.cells = items.cells
			self.cellTypes = items.cellsTypes
			for item in items.recipients {
				let item = Item(id: +1, name: item.recipient)
				self.recipients.append(item)
			}
			if self.stateOfController == AddNewParcelState.newPackage {
				self.updateAccordingToCellType()
			}
		}
	}

    func setupViews() {

        view.addSubview(scrollView)
        guard let containerView = scrollView.containerView else { return }
		
		containerView.addSubview(idTitleLabel)
		containerView.addSubview(idValueLabel)
		containerView.addSubview(idSeparatorView)
        containerView.addSubview(barcodeTitleLabel)
        containerView.addSubview(barcodeSelectButton)
        containerView.addSubview(firstSeparatorView)
        containerView.addSubview(recepientTextField)
        containerView.addSubview(barcodeTextField)
        containerView.addSubview(secondSeparatorView)
        containerView.addSubview(thirdSeparatorView)
        containerView.addSubview(fourstSeparatorView)
        containerView.addSubview(fivestSeparatorView)
        containerView.addSubview(storeLabel)
        containerView.addSubview(storeTextField)
        containerView.addSubview(storeSelectButton)
        containerView.addSubview(recipientLabel)
        containerView.addSubview(recipientSelectButton)
        containerView.addSubview(cellIdLabel)
        containerView.addSubview(cellSelectButton)
        containerView.addSubview(weightLabel)
        containerView.addSubview(weightTextField)
        containerView.addSubview(printButton)
        containerView.addSubview(imagePackageLabelButton)
        containerView.addSubview(labelForImagePackageLabel)
        containerView.addSubview(imagePackageButton)
        containerView.addSubview(labelForImagePackage)
        containerView.addSubview(collectionView)
		containerView.addSubview(cellTextField)

        //setupBackView()
		setupConstraints()
    }
	
	func setupConstraints() {
		setupIDViews()
		setupStoreLabel()
		setupStoreSelectButton()
		setupBarcodeLabel()
		setupValueIdLabel()
		setupRecipientLabel()
		setupRecipientSelectButton()
		setupCellIdLabel()
		setupCellSelectButton()
		setupWeightLabel()
		setupWeightTextField()
		setupPrintButton()
		setupImagePackageLabel()
		setupLabelForImagePackageLabel()
		setupImagePackage()
		setupLabelForImagePackage()
		setupCollectionView()
		setupRecepientTextField()
		setupBarcodeTextField()
		setupStoreTextField()
		setupSeparatorView()
	}

    func addTextFieldForCell(cellSelectButtonIsHidden: Bool) {
		DispatchQueue.main.async {
			if cellSelectButtonIsHidden {
				self.cellSelectButton.isHidden = true
			} else {
				self.cellSelectButton.isHidden = false
				self.cellTextField.centerYAnchor.constraint(equalTo: self.cellIdLabel.centerYAnchor).isActive = true
				self.cellTextField.rightAnchor.constraint(equalTo: self.cellSelectButton.leftAnchor, constant: -20).isActive = true
			}
		}
    }
	
    func newPackageConfig() {
        setupNavigationBar()
        setupButtonsTitle()
        printButton.isHidden = true
        if recepientTextField.text != "" {
            recipientSelectButton.setTitleColor(.systemBlue, for: .normal)
        }
    }

    func detailsPackageConfig() {
        guard let containerView = scrollView.containerView else { return }
        containerView.addSubview(dateLabel)
        containerView.addSubview(dateValueLabel)
        containerView.addSubview(dateSeparatorView)

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editPackage))

        barcodeTextField.isUserInteractionEnabled = false
        storeTextField.isUserInteractionEnabled = false
        recepientTextField.isUserInteractionEnabled = false
        cellSelectButton.isUserInteractionEnabled = false
        weightTextField.isUserInteractionEnabled = false
        dateValueLabel.isUserInteractionEnabled = false
		cellTextField.isUserInteractionEnabled = false
        printButton.isHidden = false

        barcodeTextField.textAlignment = .right
        storeTextField.textAlignment = .right
        recepientTextField.textAlignment = .right
        weightTextField.textAlignment = .right
		cellTextField.textAlignment = .right

        barcodeTextField.textColor = .black
        storeTextField.textColor = .black
        recepientTextField.textColor = .black
        cellSelectButton.setTitleColor(.black, for: .normal)
        weightTextField.textColor = .black
		cellTextField.textColor = .black
		cellTextField.isHidden = true
		
		if !isEditNotSyncedPackage {
			guard let package = self.package else { return }
			idValueLabel.text = "\(package.id)"
			barcodeTextField.text = package.uuid
			storeTextField.text = package.storeName
			recepientTextField.text = package.recipientName
			weightTextField.text = package.weight
			cellTextField.text = package.warehouseCellName ?? ""
			dateValueLabel.text = setDate(timestamp: (package.createdAt)!)
			cellSelectButton.setTitle(package.warehouseCellName, for: .normal)
			
			if let additionalPhoto1 = package.compressedAdditionalPhoto1 {
				additionalPhotosCompressed.append(additionalPhoto1)
			}
			if let additionalPhoto2 = package.compressedAdditionalPhoto2 {
				additionalPhotosCompressed.append(additionalPhoto2)
			}
			if let additionalPhoto3 = package.compressedAdditionalPhoto3 {
				additionalPhotosCompressed.append(additionalPhoto3)
			}
			if let additionalPhoto4 = package.compressedAdditionalPhoto4 {
				additionalPhotosCompressed.append(additionalPhoto4)
			}
			
			
			if let additionalPhoto1 = package.additionalPhoto1 {
				additionalPhotosOriginal.append(additionalPhoto1)
			}
			if let additionalPhoto2 = package.additionalPhoto2 {
				additionalPhotosOriginal.append(additionalPhoto2)
			}
			if let additionalPhoto3 = package.additionalPhoto3 {
				additionalPhotosOriginal.append(additionalPhoto3)
			}
			if let additionalPhoto4 = package.additionalPhoto4 {
				additionalPhotosOriginal.append(additionalPhoto4)
			}

			additionalPhotosOriginal = additionalPhotosOriginal.filter({ $0 != ""})
			
			let packageImageUrl = URL(string: package.compressedPackagePhoto ?? package.packagePhoto)
			self.imagePackageButton.setTitle("", for: .normal)
			imagePackageButton.sd_setBackgroundImage(with: packageImageUrl, for: .normal) { (image, _, _, _) in
				if let image = image {
					self.packagePhotoForEditeblePackage = image.jpegData(compressionQuality: 0.4)
				}
			}
			let labelImageUrl = URL(string: (package.compressedLabelPhoto ?? package.labelPhoto)!)
			self.imagePackageLabelButton.setTitle("", for: .normal)
			imagePackageLabelButton.sd_setBackgroundImage(with: labelImageUrl, for: .normal) { (image, _, _, _) in
				if let image = image {
					self.labelPhotoForEditeblePackage = image.jpegData(compressionQuality: 0.4)
				}
			}
			
		} else {
			fillWithNewPackage(isAllowEdit: false)
		}
		collectionView.reloadData()
        overrideConstraintForDetailsVC()
    }

    func editPackageConfig() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(savePackageAfterEditing))

        guard let containerView = scrollView.containerView else { return }
        barcodeTextField.removeFromSuperview()
        containerView.addSubview(barcodeTextField)
        containerView.addSubview(barcodeSelectButton)

        storeTextField.removeFromSuperview()
        containerView.addSubview(storeTextField)
        containerView.addSubview(storeSelectButton)

        recepientTextField.removeFromSuperview()
        containerView.addSubview(recepientTextField)
        containerView.addSubview(recipientSelectButton)

        setupBarcodeTextField()
        setupValueIdLabel()
        setupStoreTextField()
        setupStoreSelectButton()
        setupRecepientTextField()
        setupRecipientSelectButton()

        barcodeTextField.isUserInteractionEnabled = true
        storeTextField.isUserInteractionEnabled = true
        recepientTextField.isUserInteractionEnabled = true
        cellSelectButton.isUserInteractionEnabled = true
        weightTextField.isUserInteractionEnabled = true
        imagePackageButton.isUserInteractionEnabled = true
        imagePackageLabelButton.isUserInteractionEnabled = true
		cellTextField.isUserInteractionEnabled = true
        printButton.isHidden = true
		cellSelectButton.setTitleColor(.systemBlue, for: .normal)
		cellTextField.isHidden = false

        barcodeTextField.textColor = .lightGray
        storeTextField.textColor = .lightGray
        recepientTextField.textColor = .lightGray
        weightTextField.textColor = .lightGray
		cellTextField.textColor = .lightGray
		cellSelectButton.setTitle("-->", for: .normal)
		if !isEditNotSyncedPackage {
			if let store = AddNewParcelViewController.storeForEditeblePackage {
				storeTextField.text = store
			}
			if let recepient = AddNewParcelViewController.recipientForEditeblePackage {
				recepientTextField.text = recepient
			}
			if let cellID = AddNewParcelViewController.cellIdForEditeblePackage {
				cellTextField.text = "\(cellID)"
			} else if let cellName = AddNewParcelViewController.cellForEditeblePackage {
				cellTextField.text = cellName
			}
		} else {
			fillWithNewPackage(isAllowEdit: true)
		}
    }

    func overrideConstraintForDetailsVC() {
        guard let containerView = scrollView.containerView else { return }
        barcodeSelectButton.removeFromSuperview()
        barcodeTextField.centerYAnchor.constraint(equalTo: barcodeTitleLabel.centerYAnchor).isActive = true
		barcodeTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 0.6).isActive = true
        barcodeTextField.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -20).isActive = true

        dateLabel.topAnchor.constraint(equalTo: barcodeTitleLabel.bottomAnchor, constant: topSpacing).isActive = true
        dateLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        dateLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true

        dateValueLabel.centerYAnchor.constraint(equalTo: dateLabel.centerYAnchor).isActive = true
        dateValueLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true

        storeSelectButton.removeFromSuperview()
        storeLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: topSpacing).isActive = true
        storeTextField.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true
        storeTextField.centerYAnchor.constraint(equalTo: storeLabel.centerYAnchor).isActive = true

        recipientSelectButton.removeFromSuperview()
        recepientTextField.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true

        dateSeparatorView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 10).isActive = true
        dateSeparatorView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        dateSeparatorView.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true
        dateSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

    func setupSeparatorView() {
        guard let containerView = scrollView.containerView else { return }
		if stateOfController != .newPackage {
			idSeparatorView.topAnchor.constraint(equalTo: idValueLabel.bottomAnchor, constant: 5).isActive = true
			idSeparatorView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
			idSeparatorView.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true
			idSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
		}
        firstSeparatorView.topAnchor.constraint(equalTo: barcodeTitleLabel.bottomAnchor, constant: 5).isActive = true
        firstSeparatorView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        firstSeparatorView.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true
        firstSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true

        secondSeparatorView.topAnchor.constraint(equalTo: storeLabel.bottomAnchor, constant: 5).isActive = true
        secondSeparatorView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        secondSeparatorView.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true
        secondSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true

        thirdSeparatorView.topAnchor.constraint(equalTo: recipientLabel.bottomAnchor, constant: 5).isActive = true
        thirdSeparatorView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        thirdSeparatorView.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true
        thirdSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true

        fourstSeparatorView.topAnchor.constraint(equalTo: cellIdLabel.bottomAnchor, constant: 5).isActive = true
        fourstSeparatorView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        fourstSeparatorView.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true
        fourstSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true

        fivestSeparatorView.topAnchor.constraint(equalTo: weightLabel.bottomAnchor, constant: 5).isActive = true
        fivestSeparatorView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        fivestSeparatorView.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true
        fivestSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

    func setDate(timestamp: String) -> String {
        var localDate: String = ""
        if let timeResult = Double(timestamp) {
            let date = Date(timeIntervalSince1970: timeResult)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy, HH:mm:ss"
            dateFormatter.timeZone = .current
            localDate = dateFormatter.string(from: date)
        }
        return localDate
    }
	
	private func updateAccordingToCellType() {
		if cellTypes.contains(CellsType(id: 3, type: "custom")) &&
			(cellTypes.contains(CellsType(id: 1, type: "selected")) || cellTypes.contains(CellsType(id: 2, type: "n/a"))) {
			self.addTextFieldForCell(cellSelectButtonIsHidden: false)
		} else if cellTypes.contains(CellsType(id: 3, type: "custom")) {
			self.addTextFieldForCell(cellSelectButtonIsHidden: true)
		} else if !cellTypes.contains(CellsType(id: 3, type: "custom")) { }
	}

    @objc func editPackage() {
		for (index, _) in additionalPhotosForEditablePackage.enumerated() {
			additionalPhotosForEditablePackage[index] = nil
		}
		for (index, _) in additionalPhotosDataForEditablePackage.enumerated() {
			additionalPhotosDataForEditablePackage[index] = nil
		}
        stateOfController = .editPackage
		updateAccordingToCellType()
        if let state = stateOfController {
			collectionView.reloadData()
            update(state: state)
        }
    }

    @objc func savePackageAfterEditing() {
		if detailsobjId.isEmpty {
			saveNewPackage()
			return
		}
		
        if let store = storeTextField.text,
           let recepient = recepientTextField.text,
           let packageUuid = barcodeTextField.text,
           let weight = weightTextField.text {
			ProgressManager.show()
			var editablePackage: NewPackage = NewPackage(store: store,
                                                         warehouseCellId: AddNewParcelViewController.cellIdForEditeblePackage ?? 0,
														 cellName: AddNewParcelViewController.cellForEditeblePackage,
														 uuid: packageUuid,
														 recipient: recepient,
														 weight: weight,
                                                         packagePhoto: packagePhotoForEditeblePackage,
                                                         labelPhoto: labelPhotoForEditeblePackage,
                                                         additionalPhoto1: additionalPhotosDataForEditablePackage[0],
                                                         additionalPhoto2: additionalPhotosDataForEditablePackage[1],
                                                         additionalPhoto3: additionalPhotosDataForEditablePackage[2],
                                                         additionalPhoto4: additionalPhotosDataForEditablePackage[3])
			APIService.shared.updatePackageById(id: detailsobjId, withCellName: AddNewParcelViewController.cellIdForEditeblePackage == nil, package: editablePackage) { [weak self] error in
				guard let `self` = self else { return }
				ProgressManager.success()
                if error == nil {
//					self.stateOfController = .showDetailsPackage
//					self.additionalPhotos = []
//					if let state = self.stateOfController {
//						self.update(state: state)
//                    }
					var notSyncedObjects = BCDataManager.shared.getNotSyncedPackages() ?? []
					notSyncedObjects.removeAll(where: {$0.editId == editablePackage.editId})
					notSyncedObjects.append(editablePackage)
					self.newPackage = editablePackage
					self.loadPackageById(id: self.detailsobjId)
					self.delegate?.updateWhenAppear()
                } else {
						if let afError = error!.asAFError {
							switch afError {
							case .sessionTaskFailed(let sessionError):
								if let urlError = sessionError as? URLError {
									print("Internet connection error")
									let alert = UIAlertController(title: "Нет интернет соединения", message: "Данные синхронизируются позже", preferredStyle: .alert)
									var notSyncedObjects = BCDataManager.shared.getNotSyncedPackages() ?? []
									editablePackage.isEdit = true
									editablePackage.editId = !self.detailsobjId.isEmpty ? Int(self.detailsobjId)! : nil
									editablePackage.packagePhotoUrl = self.package?.packagePhoto
									editablePackage.labelPhotoUrl = self.package?.labelPhoto
									editablePackage.additionalPhoto1Url = self.package?.additionalPhoto1
									editablePackage.additionalPhoto2Url = self.package?.additionalPhoto2
									editablePackage.additionalPhoto3Url = self.package?.additionalPhoto3
									editablePackage.additionalPhoto4Url = self.package?.additionalPhoto4
									editablePackage.createdAt = self.package?.createdAt
									notSyncedObjects.removeAll(where: {$0.editId == editablePackage.editId})
									notSyncedObjects.append(editablePackage)
									self.newPackage = editablePackage
									BCDataManager.shared.saveNotSyncedPackages(parcels: notSyncedObjects)
									alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (_) in
										self.navigationToMainScreen()
										self.delegate?.updateWhenAppear()
									}))
									self.present(alert, animated: true, completion: nil)
								}
							default: return
							}
						}
                }
            }
        }
    }
	
	private func loadPackageById(id: String) {
		ProgressManager.show()
		APIService.shared.getPackageById(id: id) { [weak self] (result) in
			ProgressManager.success()
			guard let `self` = self else { return }
			switch result {
			case .success(let package):
				self.stateOfController = .showDetailsPackage
				self.package = package
				self.setupConstraints()
				self.update(state: self.stateOfController!)
				
			case .failure(let error):
				self.showAlert(message: error.localizedDescription) { (_) in
					self.navigationToMainScreen()
				}
			}
		}
	}

    @objc func selectStoreTapped() {
        let alert = UIAlertController(title: "Выберите опцию", message: "", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "С картинки", style: .default , handler:{ (UIAlertAction)in
            self.selectShopTapped()
        }))

        alert.addAction(UIAlertAction(title: "Со списка", style: .default , handler:{ (UIAlertAction)in
            self.navigationToChoiceItemScreen(choosedItemName: ChoosedItemNameEnum.store.rawValue)
        }))

        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in

        }))

        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }

    @objc func selectRecipientTapped() {
        let alert = UIAlertController(title: "Выберите опцию", message: "", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "С картинки", style: .default , handler:{ (UIAlertAction)in
            self.selectBarcodeTapped()
        }))

        alert.addAction(UIAlertAction(title: "Со списка", style: .default , handler:{ (UIAlertAction)in
            self.navigationToChoiceItemScreen(choosedItemName: ChoosedItemNameEnum.recipients.rawValue)
        }))

        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in

        }))

        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
	
	@objc func selectBarcodeTapped() {
		let storyboard = UIStoryboard(name: "ScanLabel", bundle: nil)
		let scanVC = storyboard.instantiateInitialViewController() as! ScanLabelViewController
		scanVC.delegate = self
		scanVC.imageSelected = imagePackageLabelButton.image(for: .normal)
		self.navigationController?.pushViewController(scanVC, animated: true)
	}
	
	@objc func selectShopTapped() {
		let storyboard = UIStoryboard(name: "ScanLabel", bundle: nil)
		let scanVC = storyboard.instantiateInitialViewController() as! ScanLabelViewController
		scanVC.isShopScan = true
		scanVC.delegate = self
		scanVC.imageSelected = imagePackageLabelButton.image(for: .normal)
		self.navigationController?.pushViewController(scanVC, animated: true)
	}

    @objc func selectCellTapped() {
        navigationToChoiceItemScreen(choosedItemName: ChoosedItemNameEnum.cells.rawValue)
    }

    @objc func weightTextFieldDidChange() {
        weightTextField.text = "\(weightTextField.text ?? "0")"
        newPackage.weight = "\(weightTextField.text ?? "0")"
        weightTextField.textColor = .systemGray
    }
	
	@objc func recepientTextFieldDidChange() {
		newPackage.recipient = "\(recepientTextField.text ?? "")"
		weightTextField.textColor = .systemGray
	}
	
	@objc func barcodeTextFieldDidChange() {
		newPackage.uuid = "\(barcodeTextField.text ?? "")"
	}

    @objc func storeTextFieldDidChange() {
        newPackage.store = "\(storeTextField.text ?? "")"
    }

    @objc func cellTextFieldDidChange() {
		if stateOfController! == .editPackage {
			AddNewParcelViewController.cellIdForEditeblePackage = nil
			AddNewParcelViewController.cellForEditeblePackage = "\(cellTextField.text ?? "")"
		} else {
			newPackage.cellName = "\(cellTextField.text ?? "")"
		}
    }
	
	@objc func buttonPrintPressed() {
		if let _ = defaultPeripheralID {
			self.bluetoothManager.setup(pakage: self.package!)
			self.bluetoothManager.delegate = self
			ProgressManager.show()
		} else {
			let printStoryboard = UIStoryboard(name: "Print", bundle: nil)
			let printVC = printStoryboard.instantiateInitialViewController()! as PrintViewController
			printVC.package = package!
			navigationController?.pushViewController(printVC, animated: true)
		}
	}

    @objc func setImageForPackageLabel() {
        if stateOfController == AddNewParcelState.showDetailsPackage {
			let vc = ShowImageViewController()
			vc.imageUrl = package?.labelPhoto ?? ""
			navigationController?.pushViewController(vc, animated: true)

        } else {
            showImagePickerControllerActionSheet()
            imageForPackage = "setImageForPackageLabel"
        }
    }

    @objc func setImageForPackage() {
        if stateOfController == AddNewParcelState.showDetailsPackage {
            indexOfClickedImage = 1
			let vc = ShowImageViewController()
			vc.imageUrl = package?.packagePhoto ?? ""
			navigationController?.pushViewController(vc, animated: true)

        } else {
            showImagePickerControllerActionSheet()
            imageForPackage = "setImageForPackage"
        }
    }

    func navigationToChoiceItemScreen(choosedItemName: String) {
        let chooseItemVC = storyboard?.instantiateViewController(identifier: "ChoiceItemsViewController") as! ChoiceItemsViewController
        chooseItemVC.choosingItemName = choosedItemName
        chooseItemVC.stateOfPreviousController = stateOfController
		chooseItemVC.parentController = self
        switch choosedItemName {
        case ChoosedItemNameEnum.store.rawValue:
            chooseItemVC.data = stores
        case ChoosedItemNameEnum.recipients.rawValue:
            chooseItemVC.data = recipients
        case ChoosedItemNameEnum.cells.rawValue:
            chooseItemVC.data = cells
        default:
            break
        }
        navigationController?.pushViewController(chooseItemVC, animated: true)
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = "Добавление посылки"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.systemBlue]
        let saveButtonImage = UIImage(systemName: "checkmark")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: saveButtonImage, style: .plain, target: self, action: #selector(saveNewPackage))
    }

    @objc func saveNewPackage() {
        if isValidatedFields() {
            ProgressManager.show()
            addAdditionalPhotos()
            var isCellNameExist: Bool = false
            if newPackage.warehouseCellId == 0 {
                isCellNameExist = true
            } else {
                isCellNameExist = false
            }
            APIService.shared.createPackage(package: newPackage, withCellName: isCellNameExist) { [weak self] (id, error) in
				ProgressManager.success()
				guard let self = `self` else { return }
				if let error = error {
					if let afError = error.asAFError {
						switch afError {
						case .sessionTaskFailed(let sessionError):
							if let urlError = sessionError as? URLError {
								print("Internet connection error")
								let alert = UIAlertController(title: "Нет интернет соединения", message: "Данные синхронизируются позже", preferredStyle: .alert)
								var notSyncedObjects = BCDataManager.shared.getNotSyncedPackages() ?? []
								if notSyncedObjects.contains(where: { (package) -> Bool in
									return package.localUUID == self.newPackage.localUUID
								}) {
									let index = notSyncedObjects.firstIndex { (package) -> Bool in
										package.localUUID == self.newPackage.localUUID
									}
									notSyncedObjects[index!] = self.newPackage
								} else {
									notSyncedObjects.append(self.newPackage)
								}
								BCDataManager.shared.saveNotSyncedPackages(parcels: notSyncedObjects)
								alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (_) in
									self.navigationToMainScreen()
									self.delegate?.updateWhenAppear()
								}))
								self.present(alert, animated: true, completion: nil)
								
								
							}
						default: return
						}
						return
					} else {
						let alert = UIAlertController(title: "Ошибка!", message: error._code == 413 ? "Фото слишком больших размеров" : (error is CustomError) ? (error as! CustomError).errorDescription : error.localizedDescription, preferredStyle: .alert)
						alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
						self.present(alert, animated: true, completion: nil)
						return
					}
				} else {
					var notSyncedList = BCDataManager.shared.getNotSyncedPackages()
					if self.isEditNotSyncedPackage, notSyncedList != nil {
						notSyncedList!.removeAll(where: {$0.localUUID == self.newPackage.localUUID})
						BCDataManager.shared.saveNotSyncedPackages(parcels: notSyncedList!)
					}
					self.navigationToMainScreen()
					self.delegate?.updateWhenAppear()
				}
            }
        } else {
            let alert = UIAlertController(title: "Ошибка!", message: "Все поля обязательные для заполнения", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func isValidatedFields() -> Bool {
        if barcodeTextField.text == "" {
            barcodeTextField.attributedPlaceholder = NSAttributedString(string: "Баркод", attributes: [NSAttributedString.Key.foregroundColor: UIColor.systemRed])
            return false
        } else if storeTextField.text == "" {
            storeSelectButton.setTitleColor(.systemRed, for: .normal)
            return false
        } else if recepientTextField.text == "" {
            recipientSelectButton.setTitleColor(.systemRed, for: .normal)
            return false
        } else if cellSelectButton.titleLabel?.text == "-- >" && cellTextField.text?.isEmpty == true {
            cellSelectButton.setTitleColor(.systemRed, for: .normal)
            return false
        } else if weightTextField.text == "" {
            weightTextField.attributedPlaceholder = NSAttributedString(string: "Введите вес", attributes: [NSAttributedString.Key.foregroundColor: UIColor.systemRed])
            return false
        } else if imagePackageLabelButton.imageView?.image == nil {
            imagePackageLabelButton.setTitleColor(.systemRed, for: .normal)
            imagePackageLabelButton.layer.borderColor = #colorLiteral(red: 1, green: 0.06469210237, blue: 0.1219848767, alpha: 1)
            return false
        } else if imagePackageButton.imageView?.image == nil {
            imagePackageButton.setTitleColor(.systemRed, for: .normal)
            imagePackageButton.layer.borderColor = #colorLiteral(red: 1, green: 0.06469210237, blue: 0.1219848767, alpha: 1)
            return false
        }
        return true
    }

    func navigationToMainScreen() {
		if tabBarController?.selectedIndex != 0 {
			Constants.UI.needToReloadData = true
			tabBarController?.selectedIndex = 0
		} else {
			navigationController?.popViewController(animated: true)
		}
    }

    func getImagesOfCurrentPackage() -> [String] {
        var imageUrlsString = [String]()
        if let package = package,
           let labelPhoto = package.labelPhoto {
            imageUrlsString.append(labelPhoto)
            imageUrlsString.append(package.packagePhoto)
            imageUrlsString.append(package.additionalPhoto1 ?? "")
            imageUrlsString.append(package.additionalPhoto2 ?? "")
            imageUrlsString.append(package.additionalPhoto3 ?? "")
            imageUrlsString.append(package.additionalPhoto4 ?? "")
        }
        return imageUrlsString
    }

    func addAdditionalPhotos() {
        var additionalPhotosData: [Data] = []
        if !photos.isEmpty {
            for photo in photos {
                additionalPhotosData.append(photo.jpegData(compressionQuality: 0.4)!)
            }
        }
        switch additionalPhotosData.count {
        case 1:
            newPackage.additionalPhoto1 = additionalPhotosData[0]
        case 2:
            newPackage.additionalPhoto1 = additionalPhotosData[0]
            newPackage.additionalPhoto2 = additionalPhotosData[1]
        case 3:
            newPackage.additionalPhoto1 = additionalPhotosData[0]
            newPackage.additionalPhoto2 = additionalPhotosData[1]
            newPackage.additionalPhoto3 = additionalPhotosData[2]
        case 4:
            newPackage.additionalPhoto1 = additionalPhotosData[0]
            newPackage.additionalPhoto2 = additionalPhotosData[1]
            newPackage.additionalPhoto3 = additionalPhotosData[2]
            newPackage.additionalPhoto4 = additionalPhotosData[3]
        default:
            break
        }
    }
	
	func setupIDViews() {
		guard let containerView = scrollView.containerView else { return }
		
		idTitleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10).isActive = true
		idTitleLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
		
		idValueLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10).isActive = true
		idValueLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20).isActive = true
		
		if stateOfController != .newPackage {
			idValueLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
			idTitleLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
		} else {
			idValueLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
			idTitleLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
		}
		
		
		
	}

    func setupBarcodeLabel() {
        guard let containerView = scrollView.containerView else { return }
		var topViewAnchor = containerView.topAnchor
		if stateOfController != .newPackage {
			topViewAnchor = idSeparatorView.bottomAnchor
		}
        barcodeTitleLabel.topAnchor.constraint(equalTo: topViewAnchor, constant: 10).isActive = true
        barcodeTitleLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        barcodeTitleLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }

    func setupValueIdLabel() {
        guard let containerView = scrollView.containerView else { return }
		barcodeSelectButton.centerYAnchor.constraint(equalTo: barcodeTitleLabel.centerYAnchor).isActive = true
		barcodeSelectButton.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -20).isActive = true
		barcodeSelectButton.widthAnchor.constraint(equalToConstant: barcodeSelectButton.sizeThatFits(view.frame.size).width).isActive = true
    }
	
	func setupRecepientTextField() {
		recepientTextField.centerYAnchor.constraint(equalTo: recipientLabel.centerYAnchor).isActive = true
		recepientTextField.rightAnchor.constraint(equalTo: recipientSelectButton.leftAnchor,constant: -20).isActive = true
	}
	
    func setupBarcodeTextField() {
        barcodeTextField.centerYAnchor.constraint(equalTo: barcodeSelectButton.centerYAnchor).isActive = true
        barcodeTextField.rightAnchor.constraint(equalTo: barcodeSelectButton.leftAnchor,constant: -20).isActive = true
    }

    func setupStoreLabel() {
        guard let containerView = scrollView.containerView else { return }
        if stateOfController == AddNewParcelState.newPackage {
            storeLabel.topAnchor.constraint(equalTo: barcodeTitleLabel.bottomAnchor, constant: topSpacing).isActive = true
        }
        storeLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        storeLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
    }

    func setupStoreTextField() {
        storeTextField.centerYAnchor.constraint(equalTo: storeLabel.centerYAnchor).isActive = true
        storeTextField.rightAnchor.constraint(equalTo: storeSelectButton.leftAnchor, constant: -20).isActive = true
    }

    func setupStoreSelectButton() {
        guard let containerView = scrollView.containerView else { return }
        storeSelectButton.centerYAnchor.constraint(equalTo: storeLabel.centerYAnchor).isActive = true
        storeSelectButton.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -20).isActive = true
    }

    func setupRecipientLabel() {
        guard let containerView = scrollView.containerView else { return }
        recipientLabel.topAnchor.constraint(equalTo: storeLabel.bottomAnchor, constant: topSpacing).isActive = true
        recipientLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        recipientLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }

    func setupRecipientSelectButton() {
        guard let containerView = scrollView.containerView else { return }
        recipientSelectButton.centerYAnchor.constraint(equalTo: recipientLabel.centerYAnchor).isActive = true
        recipientSelectButton.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -20).isActive = true
		recipientSelectButton.widthAnchor.constraint(equalToConstant: recipientSelectButton.sizeThatFits(view.frame.size).width).isActive = true
    }

    func setupCellIdLabel() {
        guard let containerView = scrollView.containerView else { return }
        cellIdLabel.topAnchor.constraint(equalTo: recipientLabel.bottomAnchor, constant: topSpacing).isActive = true
        cellIdLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        cellIdLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }

    func setupCellSelectButton() {
        guard let containerView = scrollView.containerView else { return }
        cellSelectButton.centerYAnchor.constraint(equalTo: cellIdLabel.centerYAnchor).isActive = true
        cellSelectButton.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -20).isActive = true
    }

    func setupWeightLabel() {
        guard let containerView = scrollView.containerView else { return }
        weightLabel.topAnchor.constraint(equalTo: cellIdLabel.bottomAnchor, constant: topSpacing).isActive = true
        weightLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20).isActive = true
        weightLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }

    func setupWeightTextField() {
        guard let containerView = scrollView.containerView else { return }
        weightTextField.centerYAnchor.constraint(equalTo: weightLabel.centerYAnchor).isActive = true
        weightTextField.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -20).isActive = true
    }

    func setupPrintButton() {
        guard let containerView = scrollView.containerView else { return }
        printButton.topAnchor.constraint(equalTo: fivestSeparatorView.bottomAnchor, constant: 20).isActive = true
        printButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
		printButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        printButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
    }

    func setupImagePackageLabel() {
        imagePackageLabelButton.topAnchor.constraint(equalTo: printButton.bottomAnchor, constant: 20).isActive = true
        imagePackageLabelButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        imagePackageLabelButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        imagePackageLabelButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -60).isActive = true
    }

    func setupLabelForImagePackageLabel() {
        labelForImagePackageLabel.topAnchor.constraint(equalTo: imagePackageLabelButton.bottomAnchor, constant: 10).isActive = true
        labelForImagePackageLabel.centerXAnchor.constraint(equalTo: imagePackageLabelButton.centerXAnchor).isActive = true
    }

    func setupImagePackage() {
        imagePackageButton.topAnchor.constraint(equalTo: printButton.bottomAnchor, constant: 20).isActive = true
        imagePackageButton.leftAnchor.constraint(equalTo: imagePackageLabelButton.rightAnchor, constant: 20).isActive = true
        imagePackageButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        imagePackageButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
    }

    func setupLabelForImagePackage() {
        labelForImagePackage.topAnchor.constraint(equalTo: imagePackageButton.bottomAnchor, constant: 10).isActive = true
        labelForImagePackage.centerXAnchor.constraint(equalTo: imagePackageButton.centerXAnchor).isActive = true
    }

    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: "PhotoCollectionViewCell")
        collectionView.register(AddNewPhotoCell.self, forCellWithReuseIdentifier: "AddNewPhotoCell")
        collectionView.topAnchor.constraint(equalTo: labelForImagePackageLabel.bottomAnchor, constant: 20).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        collectionView.heightAnchor.constraint(equalToConstant: collectionViewHeight).isActive = true
    }

    func setupButtonsTitle() {
        if newPackage.warehouseCellId != 0 {
            var cellName: String = ""
            for item in cells {
                if item.id == Int(newPackage.warehouseCellId) {
                    cellName = item.name
                }
            }
            cellTextField.text = cellName
            cellSelectButton.setTitleColor(.systemBlue, for: .normal)
        }
		recepientTextField.text = newPackage.recipient
		barcodeTextField.text = newPackage.uuid
        storeTextField.text = newPackage.store
    }
}

extension AddNewParcelViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch stateOfController {
		case .showDetailsPackage, .editPackage:
            return additionalPhotosCompressed.count
        default:
			if newPackage.additionalPhoto1 != nil {
				return additionalPhotosForEditablePackage.filter({$0 != nil}).count
			}
            if photos.count < 4 {
                return photos.count + 1
            }
            return photos.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellID = indexPath.row < photos.count ? "PhotoCollectionViewCell" : "AddNewPhotoCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        setupCell(cell: cell, indexPath: indexPath, type: cellID)
        cell.layer.cornerRadius = 8

        return cell
    }
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath)
		let vc = ShowImageViewController()
		if let photoCell = cell as? PhotoCollectionViewCell {
			vc.image = photoCell.imageView.image
		} else if let newCell = cell as? AddNewPhotoCell {
			vc.imageUrl = newCell.imageUrl
		}
		navigationController?.pushViewController(vc, animated: true)
	}


    func setupCell(cell: UICollectionViewCell, indexPath: IndexPath, type: String) {
            switch(type) {
            case "PhotoCollectionViewCell":
                setupPhotoCell(cell: cell as! PhotoCollectionViewCell, indexPath: indexPath)
            case "AddNewPhotoCell":
                setupAddPhotoCell(cell: cell as! AddNewPhotoCell, indexPath: indexPath)
            default:
                break
            }
        }

    func setupPhotoCell(cell: PhotoCollectionViewCell, indexPath: IndexPath) {
        cell.imageView.image = photos[indexPath.row]
    }

    func setupAddPhotoCell(cell: AddNewPhotoCell, indexPath: IndexPath) {
        cell.setupAddButton()
        cell.addButton.frame.size = CGSize(width: collectionViewHeight - 20, height: collectionViewHeight - 20)
		cell.addButton.tag = indexPath.row
		cell.addButton.addTarget(self, action: #selector(showAlert(_:)), for: .touchUpInside)
		
		if stateOfController! == .showDetailsPackage {
			cell.addButton.isUserInteractionEnabled = false
		} else {
			cell.addButton.isUserInteractionEnabled = true
		}
		
        switch stateOfController {
		case .showDetailsPackage:
			let imageUrlString = additionalPhotosCompressed[indexPath.row]
			let imageUrlStringOriginal = additionalPhotosOriginal[indexPath.row]
			let imageUrl = URL(string: imageUrlString)
			cell.imageUrl = imageUrlStringOriginal
			cell.addButton.setTitle("", for: .normal)
			cell.addButton.sd_setBackgroundImage(with: imageUrl, for: .normal) { (image, _, _, _) in
				
			}
			
		case .editPackage:
			if let editedImage = additionalPhotosForEditablePackage[indexPath.row] {
				cell.addButton.setImage(editedImage, for: .normal)
				cell.addButton.clipsToBounds = true
			} else {
				let additionalPhoto = UIImageView()
				let imageUrlString = additionalPhotosCompressed[indexPath.row]
				let imageUrl = URL(string: imageUrlString)
				additionalPhoto.sd_setImage(with: imageUrl, completed: { _,_,_,_  in
					cell.addButton.setImage(additionalPhoto.image, for: .normal)
					cell.addButton.clipsToBounds = true
				})
			}
		default:
            break
        }
		if newPackage.additionalPhoto1 != nil {
			cell.addButton.setImage(additionalPhotosForEditablePackage[indexPath.row], for: .normal)
		}
    }

	@objc func showAlert(_ sender: UIButton) {
		if stateOfController! == .editPackage {
			editedOptionalImageIndex = sender.tag
		}
		showImagePickerControllerActionSheet()
	}

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionViewHeight - 20, height: collectionViewHeight - 20)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        return sectionInset
    }
}

extension AddNewParcelViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func showImagePickerControllerActionSheet() {
        let alertController = UIAlertController(title: "Add photo", message: nil, preferredStyle: .actionSheet)
        let photoLibraryAction = UIAlertAction(title: "Choose from Library", style: .default) { action in
            self.showImagePickerController(sourceType: .photoLibrary)
        }
        let cameraAction = UIAlertAction(title: "Take a photo", style: .default) { action in
            self.showImagePickerController(sourceType: .camera)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alertController.addAction(photoLibraryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)

        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }

        present(alertController, animated: true, completion: nil)
    }

    func showImagePickerController(sourceType: UIImagePickerController.SourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = sourceType
        present(imagePickerController, animated: true)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController,
                                didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])  {
        dismiss(animated: true, completion: nil)

        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            switch imageForPackage {
            case "setImageForPackageLabel":
                imagePackageLabelButton.setImage(editedImage, for: .normal)
                imagePackageLabelButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
				newPackage.labelPhoto = editedImage.jpegData(compressionQuality: 0.4)!
				labelPhotoForEditeblePackage = editedImage.jpegData(compressionQuality: 0.4)
                imageForPackage = ""
            case "setImageForPackage":
                imagePackageButton.setImage(editedImage, for: .normal)
                imagePackageButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
				newPackage.packagePhoto = editedImage.jpegData(compressionQuality: 0.4)!
				packagePhotoForEditeblePackage = editedImage.jpegData(compressionQuality: 0.4)
                imageForPackage = ""
            default:
				if stateOfController! == .editPackage {
					additionalPhotosForEditablePackage[editedOptionalImageIndex ?? 0] = editedImage
					additionalPhotosDataForEditablePackage[editedOptionalImageIndex ?? 0] = editedImage.jpegData(compressionQuality: 0.4)!
				} else {
					photos.append(editedImage)
				}
            }

        } else  if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            switch imageForPackage {
            case "setImageForPackageLabel":
                imagePackageLabelButton.setImage(originalImage, for: .normal)
                imagePackageLabelButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                newPackage.labelPhoto = originalImage.jpegData(compressionQuality: 0.4)!
				labelPhotoForEditeblePackage = originalImage.jpegData(compressionQuality: 0.4)
                imageForPackage = ""
            case "setImageForPackage":
                imagePackageButton.setImage(originalImage, for: .normal)
                imagePackageButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
				newPackage.packagePhoto = originalImage.jpegData(compressionQuality: 0.4)!
				packagePhotoForEditeblePackage = originalImage.jpegData(compressionQuality: 0.4)
                imageForPackage = ""
            default:
				if stateOfController! == .editPackage {
					additionalPhotosForEditablePackage[editedOptionalImageIndex ?? 0] = originalImage
					additionalPhotosDataForEditablePackage[editedOptionalImageIndex ?? 0] = originalImage.jpegData(compressionQuality: 0.4)!
				} else {
					photos.append(originalImage)
				}
            }
        }
        collectionView.reloadData()
    }
}

extension AddNewParcelViewController: ScanLabelDelegate {
	
	func scanDidCompleate(barcode: String, recepient: String, shop: String, image: UIImage?) {
		if !barcode.isEmpty {
			barcodeTextField.text = barcode
			newPackage.uuid = barcode
		}
		if !recepient.isEmpty {
			recepientTextField.text = recepient
			newPackage.recipient = recepient
		}
		if !shop.isEmpty {
			newPackage.store = shop
			storeTextField.text = shop
		}
		if let image = image, labelPhotoForEditeblePackage == nil, stateOfController == .newPackage {
			newPackage.labelPhoto = image.jpegData(compressionQuality: 0.4)!
			imagePackageLabelButton.setImage(image, for: .normal)
			labelPhotoForEditeblePackage = image.jpegData(compressionQuality: 0.4)
		}
		
		print(barcode + recepient)
	}
}

extension AddNewParcelViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        let countdots = (textField.text?.components(separatedBy: ".").count)! - 1
        if countdots > 0 && string == "." {
            return false
        }
        let coma = (textField.text?.components(separatedBy: ",").count)! - 1
        if coma > 0 && string == "," {
            return false
        }
        return true
    }
}

extension AddNewParcelViewController: BluetoothPrinterDelegate {
	
	func didPrint() {
		ProgressManager.success()
	}
	
	func durationPrintExpire() {
		ProgressManager.success()
		showToast(message: "Возникла ошибка")
	}
	
	
}
