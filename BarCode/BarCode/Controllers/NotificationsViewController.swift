//
//  NotificationsViewController.swift
//  BarCode
//
//  Created by Developer on 21.01.2021.
//

import UIKit

class NotificationsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        // Do any additional setup after loading the view.
    }
    

    func setupNavigationBar() {
        self.navigationItem.title = "Уведомления"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.systemBlue]
    }

}
