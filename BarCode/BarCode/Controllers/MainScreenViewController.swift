//
//  MainScreenViewController.swift
//  BarCode
//
//  Created by Developer on 20.12.2020.
//

import UIKit
import SDWebImage

class MainScreenViewController: UIViewController {

    // MARK: - Properties

    var parcels: [Package] = []
	var notSyncedParcels: [NewPackage] = []
    var refreshControl = UIRefreshControl()
	var isNeedUpdateDataAfterAppear: Bool = true
	var syncTimer: Timer?
	var isSyncProccesing: Bool = false
	var isCurrentlyShow = false

    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        collectionView.showsVerticalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        return collectionView
    } ()

    let scroollToTopButton: UIButton = {
        let button = UIButton()
        let icon = UIImage(named: "arrow")
        button.setImage(icon, for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 12
        button.isHidden = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(scroollToTop), for: .touchUpInside)
        return button
    }()

    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRefreshControl()
        view.addSubview(collectionView)
        view.addSubview(scroollToTopButton)

        setupCollectioView()
        setupScroollButton()
		NotificationCenter.default
			.addObserver(self,
						 selector: #selector(changeConnectionStatus),
						 name: .flagsChanged,
						 object: nil)
	}

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
		isCurrentlyShow = true
        setupNavigationBar()
		if isNeedUpdateDataAfterAppear || Constants.UI.needToReloadData {
			Constants.UI.needToReloadData = false
			isNeedUpdateDataAfterAppear = false
			DispatchQueue.main.async {
				self.updatePackagesWhenAppear()
			}
		}
		syncTimer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(syncPackages), userInfo: nil, repeats: true)
    }
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		isCurrentlyShow = false
		syncTimer?.invalidate()
	}
	
	@objc private func syncPackages() {
		print("syncPackages")
		
		if let unSynced = BCDataManager.shared.getNotSyncedPackages(), !unSynced.isEmpty, Network.reachability.isReachable, !isSyncProccesing, isCurrentlyShow {
			isSyncProccesing = true
			let newPackage = unSynced.last!
				var isCellNameExist: Bool = false
				if newPackage.warehouseCellId == 0 {
					isCellNameExist = true
				} else {
					isCellNameExist = false
				}
			ProgressManager.show(message: "Синхронизация")
			if (newPackage.isEdit && newPackage.editId != nil) {
				APIService.shared.updatePackageById(id: "\(newPackage.editId!)", package: newPackage) {  [weak self] (error) in
					if error == nil {
						var updatedList: [NewPackage] = unSynced
						updatedList.removeAll(where: {$0.localUUID == newPackage.localUUID})
						BCDataManager.shared.saveNotSyncedPackages(parcels: updatedList)
						self?.refresh()
						self?.isSyncProccesing = false
						if updatedList.isEmpty {
							ProgressManager.success()
							self?.isSyncProccesing = false
						} else {
							self?.syncPackages()
						}
					} else {
						ProgressManager.success()
						if let customError = error as? CustomError, customError.title! == "The uuid has already been taken." {
							self?.showAlert(message: "Невозможно синхонизуваты посылку. Нужно изменить баркод", handleOk: { (_) in
								self?.isSyncProccesing = false
								self?.moveToEditNotSyncedPackage(parcel: newPackage)
							})
						}
					}
				}
			} else {
				APIService.shared.createPackage(package: newPackage, withCellName: isCellNameExist) { [weak self] (id, error)  in
					if error == nil {
						var updatedList: [NewPackage] = unSynced
						updatedList.removeAll(where: {$0.localUUID == newPackage.localUUID})
						BCDataManager.shared.saveNotSyncedPackages(parcels: updatedList)
						self?.refresh()
						self?.isSyncProccesing = false
						if updatedList.isEmpty {
							ProgressManager.success()
							self?.isSyncProccesing = false
						} else {
							self?.syncPackages()
						}
						
					} else {
						ProgressManager.success()
						if let customError = error as? CustomError {
							
							if customError.title! == "The uuid has already been taken." {
								self?.showAlert(message: "Невозможно синхонизуваты посылку. Нужно изменить баркод", handleOk: { (_) in
									self?.isSyncProccesing = false
									self?.moveToEditNotSyncedPackage(parcel: newPackage)
								})
							}
						}
					}
				}
			}
		}
	}
	
	@objc func changeConnectionStatus(_ notification: Notification) {
		syncPackages()
	}

    func setupRefreshControl() {
        collectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }

    @objc func refresh() {
		notSyncedParcels = BCDataManager.shared.getNotSyncedPackages() ?? []
		collectionView.reloadData()
        APIService.shared.getAllPackages { result in
            switch result {
            case .success(let parcels):
                self.parcels = parcels as! [Package]
				self.parcels.removeAll(where: {self.notSyncedParcels.map({$0.editId}).contains($0.id)})
                self.collectionView.reloadData()
            case .failure(let error):
				self.showToast(message: error.localizedDescription)
            }
			self.refreshControl.endRefreshing()
        }
    }

    func setupNavigationBar() {
        self.navigationItem.title = "Главный экран"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.systemBlue]
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Выйти", style: .plain, target: self, action: #selector(logoutAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(navigationToAddNewParcelScreen))
    }

    @objc func logoutAction() {
        UserDefaults.standard.set(false, forKey: "isLogin")
        UserDefaults.standard.synchronize()
        let loginVC = storyboard?.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
        loginVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(loginVC, animated: true)
    }

    @objc func navigationToAddNewParcelScreen() {
        let addNewVC = storyboard?.instantiateViewController(identifier: "AddNewParcelViewController") as! AddNewParcelViewController
        self.title = ""
		addNewVC.delegate = self
        addNewVC.stateOfController = .newPackage
        navigationController?.pushViewController(addNewVC, animated: true)
    }

    func setupCollectioView() {
        collectionView.register(ParcelCollectionViewCell.self, forCellWithReuseIdentifier: "ParcelCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -20).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
    }

    func setupScroollButton() {
        scroollToTopButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        scroollToTopButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100).isActive = true
        scroollToTopButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        scroollToTopButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }

    @objc func scroollToTop() {
        collectionView.setContentOffset(CGPoint.zero, animated: true)
        scroollToTopButton.isHidden = true
    }
}

extension MainScreenViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return parcels.count + notSyncedParcels.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 150)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 20, right: 10)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParcelCell", for: indexPath) as! ParcelCollectionViewCell
		if indexPath.row < notSyncedParcels.count {
			let parcel = notSyncedParcels[indexPath.row]
			if let packegePhoto = parcel.packagePhoto {
				let image = UIImage(data: packegePhoto)
				cell.imageView.image = image
			} else {
				let imageUrl = URL(string: parcel.packagePhotoUrl ?? "")
				cell.imageView.sd_setImage(with: imageUrl, completed: nil)
			}
			cell.idLabel.text = parcel.cellName ?? "\(parcel.warehouseCellId)"
			cell.customerNameLabel.text = parcel.recipient
			cell.numberOfCustomerCellLabel.text = parcel.editId != nil ? String(describing: parcel.editId!) : ""
			cell.noSyncedImageView.isHidden = false
		} else {
			let parcel = parcels[indexPath.row - notSyncedParcels.count]
			let imageUrl = URL(string: parcel.compressedPackagePhoto ?? parcel.packagePhoto)
			cell.imageView.sd_setImage(with: imageUrl, completed: nil)
			cell.idLabel.text = parcel.warehouseCellName
			cell.customerNameLabel.text = parcel.recipientName
			cell.numberOfCustomerCellLabel.text = "\(parcel.id)"
			cell.noSyncedImageView.isHidden = true
		}
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if indexPath.row < notSyncedParcels.count {
			let parcel = notSyncedParcels[indexPath.row]
			moveToEditNotSyncedPackage(parcel: parcel)
		} else {
			let addNewVC = storyboard?.instantiateViewController(identifier: "AddNewParcelViewController") as! AddNewParcelViewController
			addNewVC.stateOfController = .showDetailsPackage
			addNewVC.delegate = self
			addNewVC.detailsobjId = "\(parcels[indexPath.row - notSyncedParcels.count].id)"
			addNewVC.package = parcels[indexPath.row - notSyncedParcels.count]
			navigationController?.pushViewController(addNewVC, animated: true)
		}
        
    }
	
	func moveToEditNotSyncedPackage(parcel: NewPackage) {
		let addNewVC = storyboard?.instantiateViewController(identifier: "AddNewParcelViewController") as! AddNewParcelViewController
		addNewVC.stateOfController = .showDetailsPackage
		addNewVC.newPackage = parcel
		addNewVC.delegate = self
		addNewVC.detailsobjId = parcel.editId == nil ? "" : "\(parcel.editId!)"
		addNewVC.isEditNotSyncedPackage = true
		navigationController?.pushViewController(addNewVC, animated: true)
	}

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageHeight = scrollView.frame.size.height
        let page = Int(floor((scrollView.contentOffset.y - pageHeight / 2) / pageHeight) + 1)

        if page >= 2 {
            scroollToTopButton.isHidden = false
        } else {
            scroollToTopButton.isHidden = true
        }
    }
	
	func updatePackagesWhenAppear() {
		parcels = BCDataManager.shared.getLocalPackages()?.packages ?? []
		notSyncedParcels = BCDataManager.shared.getNotSyncedPackages() ?? []
		parcels = parcels.sorted(by: {$0.id > $1.id})
		parcels.removeAll(where: {self.notSyncedParcels.map({$0.editId}).contains($0.id)})
		collectionView.reloadData()
		APIService.shared.getAllPackages { result in
			switch result {
			case .success(let parcels):
				self.parcels = parcels as! [Package]
				self.parcels.removeAll(where: {self.notSyncedParcels.map({$0.editId}).contains($0.id)})
				self.collectionView.reloadData()
			case .failure(let error): break
//				self.lostInternetConnectionAlert(message: error.localizedDescription)
			}
		}
	}
}

extension MainScreenViewController: MainScreenUpdateDelegate {
	
	func updateWhenAppear() {
		isNeedUpdateDataAfterAppear = true
	}
}
